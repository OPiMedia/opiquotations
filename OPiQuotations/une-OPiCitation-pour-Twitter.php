<?php /* -*- coding: utf-8 -*- */

/** \file une-OPiCitation-pour-Twitter.php
 * (June 28, 2018)
 *
 * \brief
 * Little PHP application to send one quotation (from OPiCitations)
 * to Twitter account https://twitter.com/OPiCitationJour
 *
 * Use the https://github.com/abraham/twitteroauth library.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014, 2016, 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiCitations
 */

set_include_path(get_include_path().PATH_SEPARATOR.dirname(realpath(__FILE__)));
require_once 'OPiQuotations/log.inc';

#DEBUG
if (true) {
    // Development configuration
    ini_set('display_errors', 'stdout');
    ini_set('display_startup_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, true);
    assert_options(ASSERT_WARNING, true);
    assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
    // Production configuration
    ini_set('display_errors', 'stderr');
    ini_set('display_startup_errors', 0);
    ini_set('html_errors', 0);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, false);
    assert_options(ASSERT_WARNING, false);
    assert_options(ASSERT_BAIL, false);

    set_error_handler('\OPiQuotations\error_handler');
#DEBUG
}
#DEBUG_END

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');

require_once 'OPiQuotations/OPiQuotations.inc';


$LABEL = 'Twitter - OPiCitationJour';

// Choose a quotation at random
$opiquotations = new OPiQuotations\OPiQuotations();

$quot = $opiquotations->quotation_by_random($LABEL);
#DEBUG
#$quot = $opiquotations->quotation_by_id(9);
#$quot = $opiquotations->quotation_by_id(110);
#$quot = $opiquotations->quotation_by_id(459);
#$quot = $opiquotations->quotation_by_id(961);
#$quot = $opiquotations->quotation_by_id(3062);
#DEBUG_END

if ($quot === null) {
    OPiQuotations\to_log('une-OPiCitation-pour-Twitter.php: no random quotation!');

    return false;
}


// Connect to Twitter
require_once('libs/twitteroauth/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;

require_once('.private/Twitter_login.inc');

$connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);

$result = $connection->get('help/configuration');
$short_url_length = (empty($result->errors)
                     ? $result->short_url_length
                     : 23);
#DEBUG
if (true) {  // print connection result for debugging
    echo '<pre>help/configuration: ';
    var_dump($result);
    echo '</pre>';
}
#DEBUG_END


$max_length = 280;

for ($try = 0; $try < 5; ++$try, --$max_length) {  // try 5 times with decreasing length
  // Build text and send to Twitter
  $text = $quot->to_text_twitter('http://www.opimedia.be/OPiCitations/?id='.$quot->id(),
                                 $short_url_length, $max_length, 'fr');

#DEBUG
  if (true) {  // print short quotation for debugging
    preg_match('/(http:\/\/.*?)(\n|$)/', $text, $long_url);
    $long_url = $long_url[0];
    echo mb_strlen($text).' - '.mb_strlen($long_url).' + '.$short_url_length.' = '.(mb_strlen($text) - mb_strlen($long_url) + $short_url_length)
      .'<pre>'.htmlspecialchars($text).'</pre>';

    unset($long_url);

    return false;  // exit to *don't* post the tweet when do tests
  }
#DEBUG_END


  // Send to Twitter
  $result = $connection->post('statuses/update', array('status' => $text,
                                                       'trim_user' => true));
  if (empty($result->errors)) {  // tweet succeed
    break;
  }
}

if (!empty($result->errors)) {  // tweet failed
    #DEBUG
    if (true) {  // print Twitter errors for debugging
        echo '<pre>$result->errors:
';
        var_dump($result->errors);
        echo '</pre>';
    }
    #DEBUG_END
    $a = array(basename(__FILE__).' error(s):');
    foreach ($result->errors as $error) {
        $a[] = '  - Error '.$error->code.': '.$error->message;
    }
    $a[] = 'Quotation '.$quot->id().': '.$text;

    OPiQuotations\to_log(implode('
', $a));
}


if (empty($result->id)) {
    OPiQuotations\to_log('une-OPiCitation-pour-Twitter.php: no add selection! '.$quot->id());
}
else {
    // Update DB with URL
    $url = 'https://twitter.com/OPiCitationJour/status/'.$result->id;
    #DEBUG
    if (true) {
        echo '<pre>id:
';
        var_dump($result->id);
        echo '</pre><a href="', $url, '">', $url, '</a>';
    }
    #DEBUG_END

    $selection = new OPiQuotations\Selection($LABEL, NULL, $url);

    $ok = $opiquotations->quotation_add_selection($quot, $selection);

    if (!$ok) {
        OPiQuotations\to_log('une-OPiCitation-pour-Twitter.php: add selection failed! '.$quot->id());
    }
}

?>