/* -*- coding: utf-8 -*- */
/** \file one-OPiQuotation.js
 * (September 11, 2016)
 *
 * \brief
 * JavaScript functions to open/close AJAX sharing panel.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014, 2015, 2016 Olivier Pirson
 * http://www.opimedia.be/
 */

/**
 * \brief
 * Close this sharing panel.
 *
 * @param: DOM liCloseItem
 */
function sharingClose(liCloseItem) {
    "use strict";

    console.assert(liCloseItem instanceof HTMLElement, liCloseItem);

    var ul = liCloseItem.parentNode;

    ul.parentNode.removeChild(ul);
}


/**
 * \brief
 * Open the sharing panel for the quotation id
 * in a <ul> sibling of item.
 *
 * @param DOM item
 * @param integer id > 0
 */
function sharingOpen(item, id) {
    "use strict";

    console.assert(item instanceof HTMLElement, item);
    console.assert(Number.isInteger(id), id);
    console.assert(id > 0, id);

    if (!item || !id || (id <= 0)) {
        return;
    }

    var parent = item.parentNode;

    // If panel already open, then remove sibling items of item
    if (parent.children.length > 1) {
        while (parent.children.length > 1) {
            parent.removeChild(parent.children[1]);
        }

        return;
    }

    // Create <ul> and fill it in AJAX
    var ul = document.createElement("ul");

    ul.setAttribute("aria-haspopup", "true");

    ul.innerHTML = "<li>Chargement&hellip;</li>";
    parent.appendChild(ul);

    var xhr = new XMLHttpRequest();

    xhr.open("GET", "OPiQuotations-sharing.php?id=" + id, true);
    xhr.setRequestHeader("Content-Type", "text/html;charset=UTF-8");

    xhr.onreadystatechange = function() {  // callback
        if ((xhr.readyState !== 4) || (xhr.status !== 200 || !xhr.responseText)) {
            return;
        }
        ul.innerHTML = xhr.responseText;
    };

    xhr.send(null);
}
