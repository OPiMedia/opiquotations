/* -*- coding: utf-8 -*- */
/** \file OPiQuotations.js
 * (September 11, 2016)
 *
 * \brief
 * JavaScript functions to
 *   - highlight founded words
 *   - init sharing buttons
 *   - open/close about panel
 *   - open/close AJAX lists
 *   - open/close AJAX sharing panel
 *   - open/close control panel.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014, 2015, 2016 Olivier Pirson
 * http://www.opimedia.be/
 */

/**
 * \brief
 * true if highlight is active,
 * else false.
 */
var highlightActive = false;


/**
 * \brief
 * The stylesheet to modify.
 */
var highlightSheet = null;



/**
 * \brief
 * Close the about panel.
 */
function aboutClose() {
    "use strict";

    cssClassRemove(document.getElementById("about"), "opened");
}


/**
 * \brief
 * Open the about panel.
 */
function aboutOpen() {
    "use strict";

    cssClassAdd(document.getElementById("about"), "opened");
}


/**
 * \brief
 * AJAX load the list name, if necessary.
 *
 * @param String name
 */
function buildList(name) {
    "use strict";

    console.assert(typeof name === "string", name);

    if (document.getElementById("list-" + name + "-list") !== null) {  // already loaded
        return;
    }

    var xhr = new XMLHttpRequest();

    xhr.open("GET", "OPiQuotations-list.php?list=" + name + "s", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xhr.onreadystatechange = function() {  // callback
        if ((xhr.readyState !== 4) || (xhr.status !== 200)) {
            return;
        }

        var list = JSON.parse(xhr.responseText);
        var containerItem = document.getElementById("list-" + name + "-container");

        if (!list || (list.length === 0)) {
            containerItem.innerHTML = "";  // remove loading message

            return;
        }

        // Add number of items
        document.getElementById("list-" + name + "-nb").innerHTML = list.length;

        // Calculate min and max of occurrences
        var nbOccurrence;
        var min = Number.MAX_VALUE;
        var max = 0;
        var i;

        for (i in list) {
            nbOccurrence = list[i][1];
            min = Math.min(min, nbOccurrence);
            max = Math.max(max, nbOccurrence);
        }

        // Build the list
        var ulItem = document.createElement("ul");

        ulItem.setAttribute("id", "list-" + name + "-list");

        var aItem;
        var value;
        var percent;

        for (i in list) {
            aItem = document.createElement("a");

            value = list[i][0];
            nbOccurrence = list[i][1];

            percent = (nbOccurrence - min)*120/(max - min) + 80;  // between 80% and 200%

            cssClassAdd(aItem, (nbOccurrence === min
                                ? "min"
                                : (nbOccurrence === max
                                   ? "max"
                                   : "")));

            aItem.setAttribute("href", encodeURI("?" + name + "=" + value));
            aItem.setAttribute("title", nbOccurrence + " citation" + (nbOccurrence > 1
                                                                      ? "s"
                                                                      : ""));
            aItem.style.fontSize = Math.round(percent) + "%";

            aItem.appendChild(document.createTextNode(value));

            ulItem.appendChild(document.createElement("li")).appendChild(aItem);
        }

        containerItem.innerHTML = "";  // remove loading message
        containerItem.appendChild(ulItem);
    };

    xhr.send(null);
}


/**
 * \brief
 * Close the control panel (in responsive mode),
 * and eventually the about panel.
 */
function controlPanelClose() {
    "use strict";

    cssClassRemove(document.getElementById("control-panel"), "opened");
    aboutClose();
}


/**
 * \brief
 * Open the control panel (in responsive mode).
 */
function controlPanelOpen() {
    "use strict";

    cssClassAdd(document.getElementById("control-panel"), "opened");
}


/**
 * \brief
 * Add a CSS class to item.
 *
 * @param DOM item
 * @param String className
 */
function cssClassAdd(item, className) {
    "use strict";

    console.assert(item instanceof HTMLElement, item);
    console.assert(typeof className  === "string", className);

    if (className !== "") {
        try {
            item.classList.add(className);
        }
        catch (e) {  // some old browsers and IE < 10
            item.className += " " + className;
        }
    }
}


/**
 * \brief
 * Remove a CSS class to item.
 *
 * @param DOM item
 * @param String className
 */
function cssClassRemove(item, className) {
    "use strict";

    console.assert(item instanceof HTMLElement, item);
    console.assert(typeof className  === "string", className);

    if (className !== "") {
        try {
            item.classList.remove(className);
        }
        catch (e) {  // some old browsers and IE < 10
            var a = item.className.split(" ");
            var i = a.indexOf(className);

            if (i >= 0) {
                a.splice(i, 1);
            }
            item.className = a.join(" ");
        }
    }
}


/**
 * \brief
 * Switch on/off all highlighted words
 * and close the control panel (in responsive mode).
 */
function highlightOnOff() {
    "use strict";

    if (highlightSheet !== null) {
        if (highlightActive) {
            highlightSheet.deleteRule(0);
            highlightSheet.deleteRule(0);
        }
        else {
            highlightSheet.insertRule("main > section.quotation mark { background-color: yellow !important; }", 0);
            highlightSheet.insertRule("main > section.maxim mark { background-color: #ffbda0 !important; }", 1);
        }
        highlightActive = !highlightActive;
    }

    controlPanelClose();
}


/**
 * \brief
 * Close all lists in panel,
 * open list of name.
 *
 * Load the list name, if necessary.
 *
 * @param String name
 */
function listOpen(name) {
    "use strict";

    console.assert(typeof name  === "string", name);

    listsHide();

    window.scroll(0, 0);
    document.getElementById("list-" + name).style.display = "block";

    buildList(name);
}


/**
 * \brief
 * Close all lists in panel.
 */
function listsHide() {
    "use strict";

    var labels = ["author", "nation", "subject", "work"];

    var i;

    for (i in labels) {
        document.getElementById("list-" + labels[i]).style.display = "none";
    }
}


/**
 * \brief
 * Close this sharing panel.
 *
 * @param: DOM liCloseItem
 */
function sharingClose(liCloseItem) {
    "use strict";

    console.assert(liCloseItem instanceof HTMLElement, liCloseItem);

    var ul = liCloseItem.parentNode;

    ul.parentNode.removeChild(ul);
}


/**
 * \brief
 * Open the sharing panel for the quotation id
 * in a <ul> sibling of item.
 *
 * @param DOM item
 * @param integer id > 0
 */
function sharingOpen(item, id) {
    "use strict";

    console.assert(item instanceof HTMLElement, item);
    console.assert(Number.isInteger(id), id);
    console.assert(id > 0, id);

    if (!item || !id || (id <= 0)) {
        return;
    }

    var parent = item.parentNode;

    // If panel already open, then remove sibling items of item
    if (parent.children.length > 1) {
        while (parent.children.length > 1) {
            parent.removeChild(parent.children[1]);
        }

        return;
    }

    // Create <ul> and fill it in AJAX
    var ul = document.createElement("ul");

    ul.setAttribute("aria-haspopup", "true");

    ul.innerHTML = "<li>Chargement&hellip;</li>";
    parent.appendChild(ul);

    var xhr = new XMLHttpRequest();

    xhr.open("GET", "OPiQuotations-sharing.php?id=" + id, true);
    xhr.setRequestHeader("Content-Type", "text/html;charset=UTF-8");

    xhr.onreadystatechange = function() {  // callback
        if ((xhr.readyState !== 4) || (xhr.status !== 200 || !xhr.responseText)) {
            return;
        }
        ul.innerHTML = xhr.responseText;
    };

    xhr.send(null);
}



/**
 * \brief
 * Initializations.
 */
(function () {
    "use strict";

    // Search the good stylesheet
    var i;
    for (i = 0; i < document.styleSheets.length; ++i) {
        highlightSheet = document.styleSheets[i];

        if (highlightSheet.title === "highlight") {
            break;
        }
    }


    // Set listener to on/off highlight
    var item = document.getElementById("highlight-on-off");

    if (item) {
        item.addEventListener("click", highlightOnOff, false);
    }

    // Set listeners to open/close about panel
    document.getElementById("about-open").addEventListener("click", aboutOpen, false);
    document.getElementById("about-close").addEventListener("click", aboutClose, false);


    // Set listeners to open/close control panel
    document.getElementById("control-panel-right-border").addEventListener("click", controlPanelOpen, false);
    document.getElementById("control-panel-close").addEventListener("click", controlPanelClose, false);


    // Set listeners to open/close lists
    var buttons = document.getElementById("lists").getElementsByClassName("close");

    for (i = 0; i < buttons.length; ++i) {
        buttons[i].addEventListener("click", listsHide, false);
    }

    buttons = document.getElementById("menu").getElementsByClassName("list-open");
    for (i = 0; i < buttons.length; ++i) {
        (function (name) {
            buttons[i].addEventListener("click",
                                        function () {
                                            listOpen(name);
                                        }, false);
        }(buttons[i].parentNode.parentNode.className));
    }
}());
