<?php /* -*- coding: utf-8 -*- */

/** \file OPiQuotation.inc
 * (January 18, 2019)
 *
 * \brief
 * Class quotation (text, author…) or maxim (text, nation…).
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014, 2015, 2016, 2017, 2018, 2019 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiQuotations
 */
namespace OPiQuotations;


/**
 * \brief
 * Class quotation (text, author…) or maxim (text, nation…).
 */
class OPiQuotation {
    /**
     * \brief
     * Construct a quotation/maxim.
     *
     * See to_html().
     *
     * @param int $id Id of the quotation (must be > 0)
     * @param string $text Text of the quotation (should not contain \htmlonly'@@@#'\endhtmlonly and \htmlonly'#@@@'\endhtmlonly)
     * @param bool $is_maxim true if maxim (of a possibly nation), false if quotation (of an possibly author)
     * @param bool $is_marked true if marked quotation/maxim, else false
     * @param null|string $translation Possibly translation of the text (should not contain \htmlonly'@@@#'\endhtmlonly and \htmlonly'#@@@'\endhtmlonly)
     * @param null|string $subject Possibly subject
     * @param null|string $nation_author Possibly nation if maxim, else possibly author
     * @param null|string $work Possibly work if quotation (must be null if $is_maxim)
     * @param null|string $text_lang Language of text
     * @param null|Selection[] $selections List of Selection
     * @param bool $is_misattributed true if the quotation/maxim is misattributed to its author, else false
     */
    public function __construct($id, $text,
                                $is_maxim=false,
                                $is_marked=false,
                                $translation=null,
                                $subject=null, $nation_author=null, $work=null,
                                $text_lang=null,
                                $selections=null,
                                $is_misattributed=false) {
        #DEBUG
        assert('is_int($id)');
        assert('$id > 0');
        assert('is_string($text)');
        assert('is_bool($is_maxim)');
        assert('is_bool($is_marked)');
        assert('($translation === null) || is_string($translation)');
        assert('($subject === null) || is_string($subject)');
        assert('($nation_author === null) || is_string($nation_author)');
        assert('($work === null) || is_string($work)');
        assert('($text_lang === null) || is_string($text_lang)');
        assert('($selections === null) || is_array($selections)');
        assert('is_bool($is_misattributed)');
        #DEBUG_END

        $this->id = $id;
        $this->text = $text;
        $this->text_lang = $text_lang;
        $this->maxim = $is_maxim;
        $this->marked = $is_marked;
        $this->translation = $translation;
        $this->subject = $subject;
        $this->misattributed = $is_misattributed;

        if ($is_maxim) {
            #DEBUG
            assert('$work === null');
            #DEBUG_END

            $this->nation = $nation_author;
        }
        else {
            $this->author = $nation_author;
            $this->work = $work;
        }

        $this->selections = ($selections === null
        ? array()
        : $selections);
    }



    /**
     * \brief
     * Return $s with each '-'
     * replaced by '\<span class"Comme-fix">-\</span>'
     * to workaround bug with this character in the Comme-Light.ttf font.
     *
     * See "Bug in dash character Comme-Light.ttf (Vernon Adams)
     * font with recent browsers"
     * http://www.opimedia.be/DS/webdev/bugs-list/Bug-in-dash-character-of-Comme-Light-ttf-Vernon-Adams-font-with-recent-browsers/
     *
     * @param string $s
     *
     * @return string
     */
    private static function _add_comme_fix($s) {
        #DEBUG
        assert('is_string($s)');
        #DEBUG_END

        // Cut on HTML tags
        $a = preg_split('/(<.*?()>)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);  // (added () in regexp to avoid bug in PHPstripDEBUG)

        // Replace each '-' not in tag
        foreach ($a as &$s) {
            if (!preg_match('/(<.*?()>)/', $s)) {  // (added () in regexp to avoid bug in PHPstripDEBUG)
                $s = preg_replace('/\-/',
                                  '<span class="Comme-fix">-</span>', $s);
            }
        }

        return implode($a);
    }



    /**
     * \brief
     * Return $s with an adding '#' character before the last word.
     *
     * @param string $s
     *
     * @return string
     */
    private static function _add_hashtag($s) {
        #DEBUG
        assert('is_string($s)');
        #DEBUG_END

        $a = explode(' ', $s);

        #DEBUG
        assert('!empty($a)');
        #DEBUG_END

        $a[count($a) - 1] = '#'.$a[count($a) - 1];

        return implode(' ', $a);
    }


    /**
     * \brief
     * Return $s formatted in HTML to be displayed.
     *
     * - HTML tags &lt;b>, &lt;c>, &lt;i>, &lt;sub>, &lt;sup> are used (the other tags are displayed as it is).
     * - The special character ¨ (diaeresis, U+00A8) is used to justify to the right.
     * - Beginning (at the beginning of the line and after an end of line) double space is used to indentation.
     *
     * @param string $s (should not contain \htmlonly'@@@#'\endhtmlonly and \htmlonly'#@@@'\endhtmlonly)
     *
     * @return string
     */
    private static function _format_correct_html($s) {
        #DEBUG
        assert('is_string($s)');
        # assert('preg_match(\'/ $/\', $s) === 0');  // check if no final space
        #DEBUG_END

        $s = preg_replace('/<(b|c|i|sub|sup|\/b|\/c|\/i|\/sub|\/sup)>/', '@@@#$1#@@@', $s);  // protect some HTML tags

        $s = htmlspecialchars($s);  // escape special HTLM characters

        // Restore HTML tags previously protected
        $s = preg_replace('/@@@#(b|i|sub|sup|\/b|\/i|\/sub|\/sup)#@@@/', '<$1>', $s);
        $s = preg_replace('/@@@#c#@@@/', '<span class="block-center">', $s);
        $s = preg_replace('/@@@#c#@@@/', '<span class="block-center">', $s);
        $s = preg_replace('/@@@#\/c#@@@\n?/', '</span>', $s);

        // Special character ¨ to justify this line to the right
        $s = preg_replace('/¨(.+)\n?/', '<span class="block-right">$1</span>', $s);

        // Non-breaking space with after « and before », ! and ?
        $s = preg_replace('/« /', '«&nbsp;', $s);
        $s = preg_replace('/ (»|!|\?)/', '&nbsp;$1', $s);

        // Non-breaking thin space before : and ;
        $s = preg_replace('/ (:|;)/', '<span class="nowrap">&thinsp;$1</span>', $s);

        // Spaces
        $s = preg_replace('/(^|\n)  /', '$1&emsp;&emsp;', $s);
        $s = preg_replace('/  /', '&nbsp;&nbsp;', $s);
        $s = preg_replace('/(^|\n) /', '&nbsp;', $s);

        // HTML tag to newline
        $s = preg_replace('/\n/', '<br>', $s);

        #DEBUG
        # assert('preg_match(\'/¨/\', $s) === 0');  // check if no character ¨
        # assert('preg_match(\'/\\\'/\', $s) === 0');  // check if no character ' (use ’ instead)
        #DEBUG_END

        return $s;
    }


    /**
     * \brief
     * If $search !== null
     * then each occurence of $search is surrounded by '&lt;mark>' and '&lt;/mark>'.
     *
     * @param string $s
     * @param null|string $search
     *
     * @return string
     */
    private static function _highlight_html($s, $search=null) {
        #DEBUG
        assert('is_string($s)');
        assert('($search === null) || is_string($search)');
        #DEBUG_END

        if ($search !== null && $search !== '') {
            $search = preg_replace('/\n/', '<br>', $search);  // split on HTML tags

            $pieces = preg_split('/(<.+?()>|&.+?;)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);  // (added () in regexp to avoid bug in PHPstripDEBUG)
            foreach ($pieces as &$piece) {
                if ((preg_match('/^<.+>$/', $piece) !== 1) && (preg_match('/^&.+;$/', $piece)) !== 1) {  // it's NOT a HTML tag or entity
                    $piece = preg_replace('/('.preg_quote($search, '/').')/i', '<mark>$1</mark>', $piece);
                }
            }

            return implode($pieces);
        }
        else {
            return $s;
        }
    }


    /**
     * \brief
     * Return a translation of the message in the language.
     *
     * If don't exist in the dictionary,
     * then return $message.
     *
     * @param string $message
     * @param string $language
     *
     * @return string
     */
    private static function _translate_message($message, $language) {
        #DEBUG
        assert('is_string($message)');
        assert('is_string($language)');
        #DEBUG_END

        $DICTIONARY = array('Language:' => array('fr' => 'Langue&thinsp;:'),
                            'Share' => array('fr' => 'Partager'),
                            'Associate link:' => array('fr' => 'Lien associé :'),

                            'maxim' => array('fr' => 'maxime'),

                            'Misattributed to' => array('fr' => 'Attribuée de façon erronée à'),

                            'en' => array('en' => 'English',
                                          'fr' => 'anglais'),
                            'de' => array('en' => 'German',
                                          'fr' => 'allemand'),
                            'el' => array('en' => 'Greek',
                                          'fr' => 'grec'),
                            'es' => array('en' => 'Spanish',
                                          'fr' => 'espagnol'),
                            'fr' => array('en' => 'French',
                                          'fr' => 'français'),
                            'la' => array('en' => 'Latin',
                                          'fr' => 'latin'),
                            'nl' => array('en' => 'Dutch',
                                          'fr' => 'néerlandais'),
                           );

        if (empty($DICTIONARY[$message])) {
            return $message;
        }

        $translations = $DICTIONARY[$message];

        return (empty($translations[$language])
        ? $message
        : $translations[$language]);
    }



    /**
     * \brief
     * Return null or the author of the quotation.
     *
     * @return null|string
     */
    public function author() {
        return ($this->maxim
                ? null
                : $this->author);
    }


    /**
     * \brief
     * Return the id of the quotation/maxim.
     *
     * @return id > 0
     */
    public function id() {
        return $this->id;
    }


    /**
     * \brief
     * Return true if is a maxim (of a possibly nation),
     * false if is a quotation (of a possibly author).
     *
     * @return bool
     */
    public function is_maxim() {
        return $this->maxim;
    }


    /**
     * \brief
     * Return true if is a marked quotation/maxim,
     * else false.
     *
     * @return bool
     */
    public function is_marked() {
        return $this->marked;
    }


    /**
     * \brief
     * Return true if is the quotation/maxim is misattributed to its author,
     * else false.
     *
     * @return bool
     */
    public function is_misattributed() {
        return $this->misattributed;
    }


    /**
     * \brief
     * Return null or the nation of the maxim.
     *
     * @return null|string
     */
    public function nation() {
        return ($this->maxim
                ? $this->nation
                : null);
    }


    /**
     * \brief
     * Return the list of Selection.
     *
     * @return Selection[]
     */
    public function selections() {
        return $this->selections;
    }


    /**
     * \brief
     * Add a Selection to the list.
     *
     * @param Selection $selection
     */
    public function selections_add($selection) {
        #DEBUG
        assert($selection instanceof Selection);
        #DEBUG_END

        $this->selections[] = $selection;
    }


    /**
     * \brief
     * Return null or the subject of the quotation/maxim.
     *
     * @return null|string
     */
    public function subject() {
        return $this->subject;
    }


    /**
     * \brief
     * Return the text of the quotation/maxim.
     *
     * @return string
     */
    public function text() {
        return $this->text;
    }


    /**
     * \brief
     * Return null or the language of the text quotation/maxim.
     *
     * @return null|string
     */
    public function text_lang() {
        return $this->text_lang;
    }


    /**
     * \brief
     * Return the complete quotation/maxim in a HTML format.
     *
     * Layout
     * - for a quotation:
     *\code
id  one_OPiQuotation_link  Share  Selections
   subject  (mark)
text | translation
            author
              work
              *\endcode
              *
              * - for a maxim:
              *\code
id  one_OPiQuotation_link  Share  Selections
   subject  (mark)
text | translation
            nation
     *\endcode
     *
     * In text and translation:
     * - HTML tags &lt;b>, &lt;c>, &lt;i>, &lt;sub>, &lt;sup> are used (the other tags are displayed as it is).
     * - The special character ¨ (diaeresis, U+00A8) is used to justify to the right.
     * - Beginning (at the beginning of the line and after an end of line) double space is used to indentation.
     *
     * If $search !== null
     * then each occurence of $search founded is surrounded by '&lt;mark>' and '&lt;/mark>'.
     *
     * If $add_link
     * then add links to id, subject, nation, author and work.
     *
     * If $link_target
     * then add a target attribut to each link.
     *
     * The subject is surrounded by $subject_tag.
     *
     * @param null|string $search
     * @param bool $add_link
     * @param null|string $link_target
     * @param string $subject_tag
     * @param string $one_OPiQuotation_link
     * @param string $language
     *
     * @return string
     */
    public function to_html($search=null, $add_link=true, $link_target=null,
                            $subject_tag='h2', $one_OPiQuotation_link='une-OPiCitation.php',
                            $language='en') {
        #DEBUG
        assert('($search === null) || is_string($search)');
        assert('is_bool($add_link)');
        assert('($link_target === null) || is_string($link_target)');
        assert('is_string($subject_tag)');
        assert('is_string($one_OPiQuotation_link)');
        assert('is_string($language)');
        #DEBUG_END

        $html_link_target = ($link_target
                             ? ' target="'.$link_target.'"'
                             : '');

        // Maxim or quotation
        $is_marked = ($this->is_marked()
                      ? ' marked'
                      : '');

        $a = array($this->is_maxim()
                   ? '<section id="maxim-'.$this->id().'" class="maxim'.$is_marked.'">'
                   : '<section id="quotation-'.$this->id().'" class="quotation'.$is_marked.'">');

        unset($is_marked);


        // Header
        $a[] = '  <header>';

        // Marked ?
        if ($this->is_marked()) {
            $a[] = '    <aside class="mark"></aside>';
        }

        // Id
        $url = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/';

        // Collates all pieces
        $a[] = '    <nav class="id">
'.($add_link
   ? '      <a href="./?id='.$this->id().'"'.$html_link_target.'>'.$this->id().'</a>
      <a rel="nofollow" class="one-OPiQuotation" href="'.$one_OPiQuotation_link.'?id='.$this->id().'" target="_blank">&#10102;</a>
      <div class="sharing"><span onclick="sharingOpen(this, '.$this->id().'); return false;">'.OPiQuotation::_translate_message('Share', $language).'</span></div>'
      .Selection::_selections_to_html_ul($this->selections(), $link_target)
   : $this->id()).'
    </nav>';

        if ($this->subject() !== null) {  // subject
            $html = OPiQuotation::_highlight_html(htmlspecialchars($this->subject()), $search);
            $a[] = '    <'.$subject_tag.' class="subject">'
              .($add_link
                ? '<a href="./?subject='.rawurlencode($this->subject()).'"'.$html_link_target.'>'.$html.'</a>'
                : $html).'</'.$subject_tag.'>';
        }
        else {
            $a[] = '    <'.$subject_tag.' class="no-display">'
              .($this->is_maxim()
                ? 'maxim'
                : 'quotation').'</'.$subject_tag.'>';
        }

        $a[] = '  </header>';


        // Text and translation
        $text_lang = ($this->text_lang() === null
                      ? ''
                      : ' lang="'.$this->text_lang().'"');
        $text_lang_title = ($this->text_lang() === null
                            ? ''
                            : ' title="'.OPiQuotation::_translate_message('Language:', $language).' '.OPiQuotation::_translate_message($this->text_lang(), $language).'"');

        if ($this->translation() === null) {  // text
            $a[] = '  <div class="text"'.$text_lang_title.'><cite'.$text_lang.'>'.OPiQuotation::_highlight_html(OPiQuotation::_format_correct_html($this->text()), $search).'</cite></div>';
        }
        else {                                // text, translation
            $a[] = '  <div class="text_translation">
    <div class="text"'.$text_lang_title.'><cite'.$text_lang.'>'.OPiQuotation::_highlight_html(OPiQuotation::_format_correct_html($this->text()), $search).'</cite></div>
    <div class="translation"><cite>'.OPiQuotation::_highlight_html(OPiQuotation::_format_correct_html($this->translation()), $search).'</cite></div>
  </div>';
        }

        unset($text_lang);
        unset($text_lang_title);


        // Footer
        $a[] = '  <footer>';

        if ($this->is_maxim()) {  // nation
            #DEBUG
            assert('$this->author() === null');
            assert('$this->work() === null');
            #DEBUG_END

            if ($this->nation() !== null) {
                $html = OPiQuotation::_highlight_html(htmlspecialchars($this->nation()), $search);
                $a[] = '    <div class="nation">'
                  .($this->is_misattributed()
                    ? '('.OPiQuotation::_translate_message('Misattributed to', $language).') '
                    : '')
                  .($add_link
                    ? '<a href="./?nation='.rawurlencode($this->nation()).'"'.$html_link_target.'>'.OPiQuotation::_translate_message('maxim', $language).' '.$html.'</a>'
                    : OPiQuotation::_translate_message('maxim', $language).' '.$html).'</div>';
            }
        }
        else {                    // author, work
            #DEBUG
            assert('$this->nation() === null');
            #DEBUG_END

            if ($this->author() !== null) {
                $html = OPiQuotation::_highlight_html(htmlspecialchars($this->author()), $search);
                $a[] = '    <div class="author">'
                  .($this->is_misattributed()
                    ? '('.OPiQuotation::_translate_message('Misattributed to', $language).') '
                    : '')
                  .($add_link
                    ? '<a href="./?author='.rawurlencode($this->author()).'"'.$html_link_target.'>'.$html.'</a>'
                    : $html).'</div>';
            }

            if ($this->work() !== null) {
                $html = OPiQuotation::_highlight_html(htmlspecialchars($this->work()), $search);
                $a[] = '    <div class="work">'
                  .($add_link
                    ? '<a href="./?work='.rawurlencode($this->work()).'"'.$html_link_target.'>'.$html.'</a>'
                    : $html).'</div>';
            }
        }

        $a[] = '  </footer>';
        $a[] = '</section>
';

        return OPiQuotation::_add_comme_fix(implode('
', $a));
    }


    /**
     * \brief
     * Return the quotation/maxim with its author/nation and work
     * in text format
     * (strip HTML tags &lt;b>, &lt;c>, &lt;i>, &lt;sub>, &lt;sup>
     * and special character ¨ (diaeresis, U+00A8)).
     *
     * If $add_hashtags
     * then add '#' character before the last word of author/nation and work.
     *
     * If $url !== ''
     * then add this URL.
     *
     * If $add_selections is not empty
     * then each add selection URL for each label.
     *
     * @param bool $add_hashtags
     * @param string $language
     * @param string $url
     * @param bool $add_subject
     * @param null|string[] $add_selections
     *
     * @return string
     */
    public function to_text($add_hashtags=false, $language='en', $url='', $add_subject=false, $add_selections=null) {
        #DEBUG
        assert('is_bool($add_hashtags)');
        assert('is_string($language)');
        assert('is_string($add_url)');
        assert('is_bool($add_subject)');
        assert('($add_selections === null) || is_array($add_selections)');
        #DEBUG_END

        $quot = trim($this->text());

        // Replace <sub> and <sup> HTML tags
        $quot = preg_replace('/<sub>(.)<\/sub>/', '_$1', $quot);
        $quot = preg_replace('/<sub>(.+?)<\/sub>/', '_{$1}', $quot);
        $quot = preg_replace('/<sup>(.)<\/sup>/', '^$1', $quot);
        $quot = preg_replace('/<sup>(.+?)<\/sup>/', '^{$1}', $quot);

        $quot = preg_replace('/<(b|c|i|sub|sup|\/b|\/c|\/i|\/sub|\/sup)>/', '', $quot);  // strip some HTML tags
        $quot = preg_replace('/¨/', '', $quot);  // strip special character ¨

        $quot = '“'.$quot.'”';

        if ($this->translation() !== null) {
            $translation = trim($this->translation());

            $translation = preg_replace('/<(b|c|i|sub|sup|\/b|\/c|\/i|\/sub|\/sup)>/', '', $translation);  // strip some HTML tags
            $translation = preg_replace('/¨/', '', $translation);  // strip special character ¨

            $quot .= '
——————————
“'.$translation.'”';

            unset($translation);
        }

        $quot .= '
';

        if ($this->is_maxim()) {
            if ($this->nation() !== null) {  // add nation
                $nation = $this->nation();
                if ($add_hashtags) {
                    $nation = OPiQuotation::_add_hashtag($nation);
                }
                if ($this->is_misattributed()) {
                  $nation = OPiQuotation::_translate_message('Misattributed to', $language).' '.$nation;
                }
                $quot .= '
('.OPiQuotation::_translate_message('maxim', $language).' '.$nation.')';
            }
        }
        else {
            if ($this->author() !== null) {
                $author = $this->author();
                if ($add_hashtags) {
                    $author = OPiQuotation::_add_hashtag($author);
                }
                if ($this->is_misattributed()) {
                  $author = OPiQuotation::_translate_message('Misattributed to', $language).' '.$author;
                }
                if ($this->work() !== null) {  // add author and work
                    $quot .= '
('.$author.'/
'.$this->work().')';
                }
                else {                         // add author
                    $quot .= '
('.$author.')';
                }
            }
            else if ($this->work() !== null) {  // add work
                $quot .= '
('.$this->work().')';
            }
        }

        if (!empty($url)) {  // add URL
            $quot .= '
'.$url;
        }

        if ($add_subject && ($this->subject() !== null)) {  // add subject
            $quot .= '
'.($add_hashtags
   ? '#'
   : '').$this->subject();
        }

        $selections = $this->selections();
        if (!empty($add_selections) && !empty($selections)) {  // add some selections
            foreach ($add_selections as $label) {
                foreach ($selections as $selection) {
                    if ($selection->label() === $label) {
                        $quot .= '
'.OPiQuotation::_translate_message('Associate link:', $language).' '.$selection->url();
                    }
                }
            }
        }

        return $quot;
    }


    /**
     * \brief
     * Return the quotation/maxim with its author/nation and work
     * in text format to post to Facebook.
     *
     * If url !== ''
     * then add this URL.
     *
     * If $add_selections is not empty
     * then each add selection URL for each label.
     *
     * @param string $url
     * @param string $language
     * @param null|string[] $add_selections
     *
     * @return string
     */
    public function to_text_facebook($url='', $language='en', $add_selections=null) {
        #DEBUG
        assert('is_string($url)');
        assert('is_string($language)');
        #DEBUG_END

        return $this->to_text(true, $language, $url, true, $add_selections);
    }


    /**
     * \brief
     * Return the quotation/maxim with its author/nation and work
     * in short text format to post to Twitter.
     *
     * If a cutting is required then reduce contiguous whitespaces.
     *
     * Cut the result to not be longer than $max_length characters.
     *
     * If url !== null
     * then add url to the end on a newline.
     *
     * If $short_url_length === null
     * then use the real length of the url,
     * else assume that the url will be replaced by a short url not be longer than $short_url_length.
     *
     * @param null|string $url
     * @param null|int $short_url_length
     * @param int $max_length (must be > 0)
     * @param string $language
     *
     * @return string
     */
    public function to_text_twitter($url=null, $short_url_length=null, $max_length=279,
                                    $language='en') {
        #DEBUG
        assert('($url === null) || is_string($url)');
        assert('($short_url_length === null) || is_int($short_url_length)');
        assert('($short_url_length === null) || ($short_url_length >= 0)');
        assert('is_int($max_length)');
        assert('$max_length > 0');
        assert('is_string($language)');
        #DEBUG_END

        $quot = $this->to_text(true, $language);

        if ($url !== null) {
            if ($short_url_length === null) {
                $short_url_length = mb_strlen($url);
            }
            $max_length -= 1 + $short_url_length;

            $url = '
'.$url;
        }

        $text = text_cut($quot, $max_length).$url;
        $subject = ($this->subject() === null
                    ? ''
                    : '
#'.$this->subject());

        return $text.(mb_strlen($quot) + mb_strlen($subject) <= $max_length
                      ? $subject
                      : '');
    }


    /**
     * \brief
     * Return null or the translation of the quotation/maxim.
     *
     * @return null|string
     */
    public function translation() {
        return $this->translation;
    }


    /**
     * \brief
     * Return null or the work of the quotation.
     *
     * @return null|string
     */
    public function work() {
        return ($this->maxim
                ? null
                : $this->work);
    }



    /** @var null|string $author
     * \brief
     * null or the author of the quotation if is a quotation,
     * else null.
     */
    protected $author;

    /** @var int $id
     * \brief
     * The id of the quotation/maxim (must be > 0).
     */
    protected $id;

    /** @var bool $is_marked
     * \brief
     * true if marked quotation/maxim,
     * else false.
     */
    protected $is_marked;

    /** @var bool $is_maxim
     * \brief
     * true if maxim (of a possibly nation),
     * false if quotation (of an possibly author).
     */
    protected $is_maxim;

    /** @var bool $is_misattributed
     * \brief
     * true if the quotation/maxim is misattributed to its author,
     * else false.
     */
    protected $is_misattributed;

    /** @var null|string $nation
     * \brief
     * null or the nation of the maxim if is a maxim,
     * else null.
     */
    protected $nation;

    /** @var Selection[] $selections
     * \brief
     * List of Selection
     */
    protected $selections;

    /** @var null|string $subject
     * \brief
     * null or the subject of the quotation/maxim.
     */
    protected $subject;

    /**
     * @var string $text
     * \brief
     * Text of the quotation/maxim.
     *
     * Maybe contains some HTML tags (see __construct()).
     */
    protected $text;

    /** @var string $text_lang
     * \brief
     * Language of text, in ISO 639-1 code:
     * https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
     *
     * Mainly used to specify language when there is a translation.
     */
    protected $text_lang;

    /**
     * @var null|string $translation
     * \brief
     * null or the translation of the quotation/maxim.
     *
     * Maybe contains some HTML tags (see __construct()).
     */
    protected $translation;

    /** @var null|string $work
     * \brief
     * null or the work of the quotation if is a quotation,
     * else null.
     */
    protected $work;
}



/**
 * \brief
 * Class selection.
 *
 * Used by applications to save quotations already chosen.
 */
class Selection {
    /**
     * \brief
     * Construct a selection.
     *
     * If $datetime_utc === null then use the current time.
     *
     * See to_html().
     *
     * @param string $label
     * @param null|DateTime $datetime_utc
     * @param null|string $url
     */
    public function __construct($label, $datetime_utc, $url=null) {
        #DEBUG
        assert('is_string($label)');
        assert(($datetime_utc === null) || $datetime_utc instanceof \DateTime);
        assert('($url === null) || is_string($url)');
        #DEBUG_END

        $this->label = $label;
        $this->datetime_utc = ($datetime_utc === null
                               ? new \DateTime('now', new \DateTimeZone('UTC'))
                               : $datetime_utc);

        $this->url = $url;
    }



    /**
     * \brief
     * Return the list of selections in a HTML format,
     * in a \<ul class="selections"> item.
     *
     * If $link_target
     * then add a target attribut to the link.
     *
     * @param array(Selection) $selections
     * @param null|string $link_target
     *
     * @return string
     */
    public static function _selections_to_html_ul($selections, $link_target=null) {
        #DEBUG
        assert('is_array($selections)');
        assert('($link_target === null) || is_string($link_target)');
        #DEBUG_END

        if (count($selections) === 0) {
            return '';
        }

        $a = [];
        foreach ($selections as $selection) {
            $a[] = $selection->to_html($link_target);
        }

        return '
      <ul class="selections">
        '.implode('
        ', $a).'
      </ul>';
    }



    /**
     * \brief
     * Return the date/time of the selection.
     *
     * @return DateTime
     */
    public function datetime_utc() {
        return $this->datetime_utc;
    }


    /**
     * \brief
     * Return the label
     * used to identify an application that already chosen a quotation.
     *
     * @return string
     */
    public function label() {
        return $this->label;
    }


    /**
     * \brief
     * Return the selection in a HTML format.
     * By default in a \<li> item.
     *
     * If $link_target
     * then add a target attribut to the link.
     *
     * @param null|string $link_target
     * @param string $tag
     *
     * @return string
     */
    public function to_html($link_target, $tag='li') {
        #DEBUG
        assert('($link_target === null) || is_string($link_target)');
        assert('is_string($tag)');
        #DEBUG_END

        $title = htmlspecialchars($this->label());
        if ($this->datetime_utc() > new \DateTime('0000-00-00')) {
            $title.= ' &mdash; '.htmlspecialchars($this->datetime_utc()->format('Y-m-d'));
        }

        $content = (preg_match('/^Web( (\d+))?/', $this->label())  // if 'Web', 'Web 2'...
                    ? ''
                    : '&#10102;');

        return '<'.$tag.' class="'.explode(' ', $this->label())[0].'" title="'.$title.'">'
            .($this->url() === null
              ? '<span>'.$content.'</span>'
              : '<a rel="nofollow" href="'.$this->url().'"'
                .($link_target
                  ? ' target="'.$link_target.'"'
                  : '').'>'.$content.'</a>')
            .'</'.$tag.'>';
    }


    /**
     * \brief
     * Return null or URL to the quotation in the application.
     *
     * @return null|string
     */
    public function url() {
        return $this->url;
    }



    /** @var string $label
     * \brief
     * Label to identify an application that already chosen a quotation.
     */
    protected $label;

    /** @var DateTime $datetime_utc
     * \brief
     * date/time of the selection.
     */
    protected $datetime_utc;

    /** @var null|string $url
     * \brief
     * null or URL to the quotation in the application.
     *
     * Like this: https://twitter.com/OPiCitationJour/status/763268655845085184
     */
    protected $url;
}


return TRUE;

?>