<?php // -*- coding: utf-8 -*-

/** \file log.inc
 * (September 3, 2016)
 *
 * \brief
 * Function to write in errors log file.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014, 2016 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiQuotations
 */
namespace OPiQuotations;


/**
 * \brief
 * Filename of errors log file.
 */
const LOG_FILE = 'logs/log.txt';



/**
 * \brief
 * Error handler function.
 *
 * Get all errors specified by error_reporting()
 * and write message with to_log() function.
 *
 * If error is an E_USER_ERROR
 * then exit,
 * else continue.
 *
 * This handler must be activated by set_error_handler('\\OPiQuotations\\error_handler').
 *
 * See http://www.php.net/manual/en/function.set-error-handler.php
 *
 * @param int $errno
 * @param string $errstr
 * @param string $errfile
 * @param int $errline
 * @param array $errcontext
 */
function error_handler($errno, $errstr, $errfile, $errline, $errcontext) {
    if (!(error_reporting() & $errno)) {  // error not specified by error_reporting()
        return false;
    }

    $exit = ($errno === E_USER_ERROR);

    $message = 'PHP error '.$errno.' "'.$errstr.'"
  line '.$errline.' in "'.$errfile.'"
  '.($exit
     ? 'exit'
     : 'continue').'
'.print_r($errcontext, true).'debug_backtrace:
'.print_r(debug_backtrace(), true);

    to_log($message);

    if ($exit) {
        exit(1);
    }

    return true;
}


/**
 * \brief
 * If $s contains an element '[password] => ...'
 * then hides the password value.
 *
 * @param string $s
 */
function hide_password($s) {
    require 'OPiQuotations/.private/db_login.inc';

    return (empty($db_password)
            ? $s
            : preg_replace('/'.preg_quote($db_password, '/').'/', '****',
                           preg_replace('/\[password\]\s*=>\s*\S+/i', '[password] => ****', $s)));
}


/**
 * \brief
 * Append $message in LOG_FILE.
 *
 * If the specified mail in '.private/log_email.inc' is not empty
 * then send also the message to this mail.
 *
 * If $message is not a string
 * then a warning is added to the message
 *      and $message is converted to a string.
 *
 * @param mixed $message
 */
function to_log($message) {
    if (!is_string($message) ) {
        $message = 'This error message wasn\'t a string!
'.print_r($message, true);
    }

    $dt = new \DateTime();

    $message = '***************************************
*** '.$dt->format('r').' ***
***************************************
'.hide_password($message).'

';

    #DEBUG
    if (true) {
        echo '<pre>to_log():
';
        var_dump($message);
        echo '</pre>';
    }
    #DEBUG_END

    error_log($message, 3, realpath(realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.LOG_FILE));

    require_once 'OPiQuotations/.private/log_email.inc';

    if (!empty($log_email)) {
        error_log($message, 1, $log_email);
    }
}


return true;

?>