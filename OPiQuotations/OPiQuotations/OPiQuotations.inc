<?php /* -*- coding: utf-8 -*- */

/** \file OPiQuotations.inc
 * (January 18, 2019)
 *
 * \brief
 * Main class.
 *
 * PHP package to deal quotations (text, author…) and maxims (text, nation…)
 * from a MySQL database.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPL3 --- Copyright (C) 2014, 2015, 2016, 2017, 2018, 2019 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @version 03.00.00 --- January 18, 2019
 * @author Olivier Pirson <olivier.pirson.opi@gmail.com>
 * @package OPiQuotations
 *
 * \mainpage OPiQuotations
 * PHP package to deal quotations (text, author…) and maxims (text, nation…)
 * from a MySQL database.
 *
 * <ul>
 *   <li>Complete sources (and MySQL quotations file) on Bitbucket: <a href="https://bitbucket.org/OPiMedia/opiquotations"><tt>https://bitbucket.org/OPiMedia/opiquotations</tt></a></li>
 *   <li><a href="http://www.opimedia.be/DS/webdev/PHP/OPiQuotations/docs/">Online HTML documentation</a></li>
 * </ul>
 *
 * <img src="OPiQuotations-64x64-t.png" width="64" height="64" border="0" alt="[OPiQuotations]" />
 *
 * My personal use of this package:
 *
 * \htmlonly
 * <a href="http://www.opimedia.be/OPiCitations/"><img src="OPiCitation-banner--577x100.jpg" width="577" height="100" border="0" alt="[OPiCitations]" /></a>
 * <ul>
 *   <li><a href="http://www.opimedia.be/OPiCitations/">O<span style="font-variant:small-caps">Pi</span>Citations <img src="OPiQuotations-16x16-t.png" width="16" height="16" border="0" alt="" /></a>: Web application containing a lot of French quotes</li>
 *   <li><a href="http://www.opimedia.be/OPiCitations/une-OPiCitation.php">une O<span style="font-variant:small-caps">Pi</span>Citation <img src="one-OPiQuotation-16x16-t.png" width="16" height="16" border="0" alt="" /></a>: Little Web application to display one OPiCitation (may be used in an iframe to include in another Web page)</li>
 *   <li><a href="https://twitter.com/OPiCitationJour"><tt>@OPiCitationJour</tt></a>: Twitter account with an O<span style="font-variant:small-caps">Pi</span>Citation by day</li>
 *   <li><a href="https://www.facebook.com/1OPiCitationParJour"><tt>1OPiCitationParJour</tt></a>: Page Facebook with another O<span style="font-variant:small-caps">Pi</span>Citation by day</li>
 * </ul>
 * \endhtmlonly
 *
 * \htmlonly
 * <hr />
 * <div>
 *   One quotation randomly chosen from the dictionary of French quotes:
 *   <a href="http://www.opimedia.be/OPiCitations/une-OPiCitation.php"><strong>une O<span style="font-variant:small-caps">Pi</span>Citations</strong> <img src="one-OPiQuotation-16x16-t.png" width="16" height="16" border="0" alt="" /></a>.
 *   <iframe src="http://www.opimedia.be/OPiCitations/une-OPiCitation.php?no-link-OPiQuotations" width="100%" height="150" scrolling="no" frameborder="0" onload="this.height=this.contentWindow.document.getElementsByTagName('body')[0].clientHeight;">[Your browser does not support iframe.]</iframe>
 * </div>
 * <hr />
 * \endhtmlonly
 *
 * <div>
 * GPLv3
 * ------
 * Copyright (C) 2014, 2015, 2016, 2017, 2018 Olivier Pirson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * </div>
 * <hr>
 *
 * \htmlonly
 * <div align="center">
 *   <a class="net" href="http://www.opimedia.be/donate/"><img src="Donate-92x26-t.png" width="92" height="26" border="0" alt="[Donate]" /></a>
 * </div>
 * \endhtmlonly
 *
 * <div align="right">
 *   &copy; Olivier <span style="font-variant:small-caps">Pirson</span>\n
 *   <a class="net" href="http://www.opimedia.be/"><tt>www.opimedia.be</tt></a>\n
 *   <a rel="nofollow" class="mail" href="mailto:olivier.pirson.opi@gmail.com?subject=[OPiQuotations]"><tt>olivier.pirson.opi@gmail.com</tt></a>
 * </div>
 */

namespace OPiQuotations;


/**
 * \brief
 * If length of $html_text <= $max_length characters
 * then return $html_text,
 * else return $html_text cutted added of $hellip to not be longer than $max_length characters.
 *
 * If a cutting is required and $reduce then reduce contiguous whitespaces.
 *
 * If the cutting cut a HTML entity
 * then remove it.
 *
 * @param string $html_text
 * @param int $max_length (must be >= length of $hellip)
 * @param string $hellip; (must not containt the & character)
 * @param bool $reduce
 *
 * @return string
 */
function html_text_cut($html_text, $max_length, $hellip='…', $reduce=true) {
    #DEBUG
    assert('is_string($html_text)');
    assert('is_int($max_length)');
    assert('is_string($hellip)');
    assert('$max_length >= mb_strlen($hellip)');
    assert('is_bool($reduce)');
    #DEBUG_END

    if (mb_strlen($html_text) > $max_length) {
        $html_text = text_cut($html_text, $max_length, $hellip, $reduce);

        $i = mb_strrpos($html_text, '&');
        if (($i !== false) && (mb_strrpos($html_text, ';', $i + 1) === false)) {
            // Remove partial HTML entity like &quot;
            $html_text = mb_substr($html_text, 0, $i).$hellip;
        }
    }

    return $html_text;
}


/**
 * \brief
 * If length of $text <= $max_length characters
 * then return $text,
 * else return $text cutted added of $hellip to be length of $max_length characters.
 *
 * If a cutting is required and $reduce then reduce contiguous whitespaces.
 *
 * @param string $text
 * @param int $max_length (must be >= length of $hellip)
 * @param string $hellip;
 * @param bool $reduce
 *
 * @return string
 */
function text_cut($text, $max_length, $hellip='…', $reduce=true) {
    #DEBUG
    assert('is_string($text)');
    assert('is_int($max_length)');
    assert('is_string($hellip)');
    assert('$max_length >= mb_strlen($hellip)');
    assert('is_bool($reduce)');
    #DEBUG_END

    if (mb_strlen($text) > $max_length) {
        if ($reduce) {
            // Reduce contiguous whitespaces with break line
            $text = preg_replace('/\s*\n\s*/', '
', $text);

            // Reduce other contiguous whitespaces (\v is ignored because confusion with \n)
            $text = preg_replace('/[ \t\r\f]+/', ' ', $text);
        }

        $text = mb_substr($text, 0, $max_length - mb_strlen($hellip)).$hellip;
    }

    return $text;
}



/**
 * \brief
 * Class to get OPiQuotation and informations from the databse.
 */
class OPiQuotations {
    /**
     * \brief
     * Connect to the database.
     * See the Db class.
     */
    public function __construct() {
        require_once 'OPiQuotations/Db.inc';
        require_once 'OPiQuotations/.private/db_login.inc';

        $this->db = new Db($db_host, $db_user, $db_password, $db_name);
    }


    /**
     * \brief
     * Return a associative table id => array(name, number of use)
     * of elements of the table author.
     *
     * @return array[array]
     */
    public function list_authors() {
        return $this->db->list_to_assoc('author');
    }


    /**
     * \brief
     * Return a associative table id => array(name, number of use)
     * of elements of the table nation.
     *
     * @return array[array]
     */
    public function list_nations() {
        return $this->db->list_to_assoc('nation');
    }


    /**
     * Return a associative table id => array(name, number of use)
     * of elements of the table subject.
     *
     * @return array[array]
     */
    public function list_subjects() {
        return $this->db->list_to_assoc('subject');
    }


    /**
     * \brief
     * Return a associative table id => array(name, number of use)
     * of elements of the table work.
     *
     * @return array[array]
     */
    public function list_works() {
        return $this->db->list_to_assoc('work');
    }


    /**
     * \brief
     * If $is_maxim === null then return the numbers of quotations/maxims,
     * if $is_maxim === false then return the numbers of quotations,
     * if $is_maxim === true then return the numbers of maxims.
     *
     * @param null|bool $is_maxim
     *
     * @return int >= 0
     */
    public function nb($is_maxim=null) {
        #DEBUG
        assert('($is_maxim === null) || is_bool($is_maxim)');
        #DEBUG_END

        return $this->db->nb($is_maxim);
    }


    /**
     * \brief
     * Add the selection to this quotation
     * and updates the DB.
     *
     * @param OPiQuotation $quotation
     * @param Selection $selection
     *
     * @return bool
     */
    public function quotation_add_selection($quotation, $selection) {
        #DEBUG
        assert($quotation instanceof OPiQuotation);
        assert($selection instanceof Selection);
        #DEBUG_END

        $data = array($quotation->id(),
                      $this->db->to_string_or_NULL($selection->label()),
                     'CONVERT_TZ('.$this->db->to_string($selection->datetime_utc()->format('Y-m-d H:i:s')).',\'+00:00\',\'+00:00\')',
                      $this->db->to_string_or_NULL($selection->url()));

        $query = 'INSERT INTO `quotation_selection` (`quotation_id`, `label`, `datetime_utc`, `url`)
VALUES ('.implode(', ', $data).');';

        return $this->db->query_insert($query);
    }


    /**
     * \brief
     * Return the unique quotation/maxim of id $id.
     *
     * If this id doesn't exist
     * then return null.
     *
     * @param int $id
     *
     * @return null|OPiQuotation
     */
    public function quotation_by_id($id) {
        #DEBUG
        assert('is_int($id)');
        #DEBUG_END

        if ($id <= 0) {
            return null;
        }

        $quots = $this->db->query_quotations('WHERE `id`='.(int)$id.'');

        return (empty($quots)
                ? null
                : $quots[0]);
    }


    /**
     * \brief
     * Return a quotation choose at random.
     * If not available quotation, then return null.
     *
     * If $label !== null
     * then choose quotation not already chosen with this label.
     *
     * @param null|string $label
     *
     * @return null|OPiQuotation
     */
    public function quotation_by_random($label=null) {
        #DEBUG
        assert('($label === null) || is_string($label)');
        #DEBUG_END

        $quots = $this->quotations_by_random(1, $label);

        return (empty($quots)
                ? null
                : $quots[0]);
    }


    /**
     * \brief
     * Return the complete list of quotations/maxims.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_all($limit=null, $offset=null) {
        #DEBUG
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('',
                                           'ORDER BY `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations/maxims.
     *
     * @return int
     */
    public function quotations_all_nb() {
        return $this->db->query_quotations_nb();
    }


    /**
     * \brief
     * Return the list of quotations/maxims that are marked.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_all_marked($limit=null, $offset=null) {
        #DEBUG
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('WHERE `is_marked`',
                                           'ORDER BY `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations/maxims that are marked.
     *
     * @return int
     */
    public function quotations_all_marked_nb() {
        return $this->db->query_quotations_nb('WHERE `is_marked`');
    }


    /**
     * \brief
     * Return the list of quotations that are written by author $author.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param string $author
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_author($author, $limit=null, $offset=null) {
        #DEBUG
        assert('is_string($author)');
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('WHERE NOT `is_maxim` AND `author` LIKE \'%'.$this->db->escape($author).'%\' COLLATE utf8_unicode_ci',
                                           'ORDER BY `author`, `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations that are written by author $author.
     *
     * @param string $author
     *
     * @return int
     */
    public function quotations_by_author_nb($author) {
        #DEBUG
        assert('is_string($author)');
        #DEBUG_END

        return $this->db->query_quotations_nb('WHERE NOT `is_maxim` AND `author` LIKE \'%'.$this->db->escape($author).'%\' COLLATE utf8_unicode_ci');
    }


    /**
     * \brief
     * Return the list of quotations/maxims of id among $ids.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param int[] $ids (each int > 0)
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_ids($ids, $limit=null, $offset=null) {
        #DEBUG
        assert('is_array($ids)');
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        foreach($ids as &$id) {
            #DEBUG
            assert('is_int($id)');
            assert('$id > 0');
            #DEBUG_END

            settype($id, 'int');
        }

        return (empty($ids)
                ? array()
                : $this->db->query_quotations('WHERE `id` IN ('.implode(',', $ids).')',
                                              '',
                                              $limit, $offset));
    }


    /**
     * \brief
     * Return the number of all quotations/maxims of id among $ids.
     *
     * @param int[] $ids (each int > 0)
     *
     * @return int
     */
    public function quotations_by_ids_nb($ids) {
        #DEBUG
        assert('is_array($ids)');
        #DEBUG_END

        foreach($ids as &$id) {
            #DEBUG
            assert('is_int($id)');
            assert('$id > 0');
            #DEBUG_END

            settype($id, 'int');
        }

        return (empty($ids)
                ? 0
                : $this->db->query_quotations_nb('WHERE `id` IN ('.implode(',', $ids).')'));
    }


    /**
     * \brief
     * Return the list of maxims of nationality $nation.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param string $nation
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_nation($nation, $limit=null, $offset=null) {
        #DEBUG
        assert('is_string($nation)');
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('WHERE `is_maxim` AND `nation` LIKE \'%'.$this->db->escape($nation).'%\' COLLATE utf8_unicode_ci',
                                           'ORDER BY `nation`, `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all maxims of nationality $nation.
     *
     * @param string $nation
     *
     * @return int
     */
    public function quotations_by_nation_nb($nation) {
        #DEBUG
        assert('is_string($nation)');
        #DEBUG_END

        return $this->db->query_quotations_nb('WHERE `is_maxim` AND `nation` LIKE \'%'.$this->db->escape($nation).'%\' COLLATE utf8_unicode_ci');
    }


    /**
     * \brief
     * Return a list of $nb different quotations (if available)
     * choose at random.
     *
     * If $label !== null
     * then choose quotations not already chosen with this label.
     *
     * @param int $nb (must be >= 0)
     * @param null|string $label
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_random($nb=1, $label=null) {
        #DEBUG
        assert('is_int($nb) && ($limit >= 0)');
        assert('$nb >= 0');
        assert('($label === null) || is_string($label)');
        #DEBUG_END

        if ($label === null) {
            $quots = $this->db->query_quotations('',
                                                 'ORDER BY RAND()', $nb);
        }
        else {
            $label = $this->db->escape($label);
            $quots = $this->db->query_quotations('WHERE `id` NOT IN (SELECT `quotation_id` FROM `quotation_selection` WHERE `label` = \''.$label.'\')',
                                                 'ORDER BY RAND()', $nb);
        }

        return $quots;
    }


    /**
     * \brief
     * Return the list of quotations/maxims are selected with $label.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param string $label
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_selection_label($label, $limit=null, $offset=null) {
        #DEBUG
        assert('is_string($label)');
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('WHERE `selection_label` = '.$this->db->to_string($label).' COLLATE utf8_unicode_ci',
                                           'ORDER BY `selection_datetime_utc` DESC, `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations/maxims are selected with $label.
     *
     * @param string $label
     *
     * @return int
     */
    public function quotations_by_selection_label_nb($label) {
        #DEBUG
        assert('is_string($label)');
        #DEBUG_END

        return $this->db->query_quotations_nb('WHERE `selection_label` = '.$this->db->to_string($label).' COLLATE utf8_unicode_ci');
    }


    /**
     * \brief
     * Return the list of quotations/maxims of subject $subject
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param string $subject
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_subject($subject, $limit=null, $offset=null) {
        #DEBUG
        assert('is_string($subject)');
        #DEBUG_END

        return $this->db->query_quotations('WHERE `subject` LIKE \'%'.$this->db->escape($subject).'%\' COLLATE utf8_unicode_ci',
                                           'ORDER BY `subject`, `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations/maxims of subject $subject
     *
     * @param string $subject
     *
     * @return int
     */
    public function quotations_by_subject_nb($subject) {
        #DEBUG
        assert('is_string($subject)');
        #DEBUG_END

        return $this->db->query_quotations_nb('WHERE `subject` LIKE \'%'.$this->db->escape($subject).'%\' COLLATE utf8_unicode_ci');
    }


    /**
     * \brief
     * Return the list of quotations/maxims that contains the text $text.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param string $text
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_text($text, $limit=null, $offset=null) {
        #DEBUG
        assert('is_string($text)');
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('WHERE `text` LIKE \'%'.$this->db->escape($text).'%\' COLLATE utf8_unicode_ci',
                                           'ORDER BY `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations/maxims that contains the text $text.
     *
     * @param string $text
     *
     * @return int
     */
    public function quotations_by_text_nb($text) {
        #DEBUG
        assert('is_string($text)');
        #DEBUG_END

        return $this->db->query_quotations_nb('WHERE `text` LIKE \'%'.$this->db->escape($text).'%\' COLLATE utf8_unicode_ci');
    }


    /**
     * \brief
     * Return the list of quotations/maxims with a translation that contains the text $translation.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param string $translation
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_translation($translation, $limit, $offset) {
        #DEBUG
        assert('is_string($translation)');
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('WHERE `translation` LIKE \'%'.$this->db->escape($translation).'%\' COLLATE utf8_unicode_ci',
                                           'ORDER BY `translation_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations/maxims with a translation that contains the text $translation.
     *
     * @param string $translation
     *
     * @return int
     */
    public function quotations_by_translation_nb($translation) {
        #DEBUG
        assert('is_string($translation)');
        #DEBUG_END

        return $this->db->query_quotations_nb('WHERE `translation` LIKE \'%'.$this->db->escape($translation).'%\' COLLATE utf8_unicode_ci');
    }


    /**
     * \brief
     * Return the list of quotations/maxims that are written in work $work.
     *
     * If $limit is not null
     * then return only $limit OPiQuotation.
     *
     * If $limit and $offset are not null
     * then return only $limit OPiQuotation from $offset.
     *
     * @param string $work
     * @param null|int $limit (must be >= 0)
     * @param null|int $offset (must be >= 0)
     *
     * @return OPiQuotation[]
     */
    public function quotations_by_work($work, $limit, $offset) {
        #DEBUG
        assert('is_string($work)');
        assert('($limit === null) || (is_int($limit) && ($limit >= 0))');
        assert('($offset === null) || (is_int($offset) && ($offset >= 0))');
        #DEBUG_END

        return $this->db->query_quotations('WHERE NOT `is_maxim` AND `work` LIKE \'%'.$this->db->escape($work).'%\' COLLATE utf8_unicode_ci',
                                           'ORDER BY `work`, `text_stripped` COLLATE utf8_unicode_ci',
                                           $limit, $offset);
    }


    /**
     * \brief
     * Return the number of all quotations/maxims that are written in work $work.
     *
     * @param string $work
     *
     * @return int
     */
    public function quotations_by_work_nb($work) {
        #DEBUG
        assert('is_string($work)');
        #DEBUG_END

        return $this->db->query_quotations_nb('WHERE NOT `is_maxim` AND `work` LIKE \'%'.$this->db->escape($work).'%\' COLLATE utf8_unicode_ci');
    }



    /** @var Db $db
     * \brief
     * Link to the database
     */
    protected $db;
}


return TRUE;

?>