<?php /* -*- coding: utf-8 -*- */

/** \file index.php
 * (April 18, 2021)
 *
 * \brief
 * Main page to the Web application OPiCitations
 * http://www.opimedia.be/OPiCitations/
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014 - 2021 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiCitations
 */

require_once 'OPiQuotations/log.inc';

#DEBUG
if (true) {
    // Development configuration
    ini_set('display_errors', 'stdout');
    ini_set('display_startup_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, true);
    assert_options(ASSERT_WARNING, true);
    assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
    // Production configuration
    ini_set('display_errors', 'stderr');
    ini_set('display_startup_errors', 0);
    ini_set('html_errors', 0);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, false);
    assert_options(ASSERT_WARNING, false);
    assert_options(ASSERT_BAIL, false);

    set_error_handler('\OPiQuotations\error_handler');
#DEBUG
}
#DEBUG_END

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');


function nb_to_nb_page($nb_quotations) {  // return the number of pages
    #DEBUG
    assert('is_int($nb_quotations) && ($nb_quotations >= 0)');
    #DEBUG_END

    global $nb_by_page;

    return ceil($nb_quotations/$nb_by_page);
}


function nb_to_offset($nb_quotations) {  // return the offset for this page
    #DEBUG
    assert('is_int($nb_quotations) && ($nb_quotations >= 0)');
    #DEBUG_END

    global $nb_by_page;
    global $num_page;

    return $nb_by_page*($num_page - 1);
}


function s($n) {  // adds 's' if plural
    #DEBUG
    assert('is_int($n) && ($n >= 0)');
    #DEBUG_END

    return ($n >= 2
            ? 's'
            : '');
}


require_once 'OPiQuotations/OPiQuotations.inc';

$opiquotations = new OPiQuotations\OPiQuotations();

$lists = array('author' => $opiquotations->list_authors(),
               'nation' => $opiquotations->list_nations(),
               'subject' => $opiquotations->list_subjects(),
               'work' => $opiquotations->list_works());

$LABELS = array('ids'         => 'numéro(s)',
                'subject'     => 'sujet',
                'text'        => 'texte',
                'translation' => 'traduction',
                'author'      => 'auteur',
                'work'        => '&oelig;uvre',
                'nation'      => 'maxime');
$ALL_KEYS = array_merge(array_keys($LABELS), ['id', 'all_marked', 'all', 'page', 'selection']);

$SELECTION_LABELS = array('Facebook' => 'Facebook - 1OPiCitationParJour',
                          'Twitter' => 'Twitter - OPiCitationJour',
                          'Web' => 'Web');


$NB_FOR_RANDOM = 10;

$nb_by_page = 100;
$num_page = 1;


// GET correct parameters
$params = array();

foreach ($_GET as $key=>$label) {
    if (in_array($key, $ALL_KEYS) && !isset($params[$key])) {
        if ($key === 'page') {
            if ($label === 'toutes') {
                $nb_by_page = 999999;
            }
            else {
                $num_page = max(1, (int)$label);
            }
        }
        else {
            $params[$key] = $_GET[$key];
        }
    }
}

unset($key);
unset($ALL_KEYS);

foreach (array('all', 'all_marked') as $key) {  // cleans values
    if (isset($_params[$key])) {
        $params[$key] = null;
    }
}

unset($key);

foreach ($params as $key=>$value) {
    if ($value !== null) {
        $params[$key] = preg_replace('/\.\.\./', '…',  # ellipsis
                                     preg_replace('/\'/', '’',  # apostrophe
                                                  trim($value)));
    }
}

unset($key);
unset($value);

$param_key = null;
$param_value = null;

if (!empty($params)) {  // get the first param
    reset($params);
    $param_key = key($params);
    $param_value = (in_array($param_key, ['all_marked', 'all'])
                    ? null
                    : $params[$param_key]);
}


// Get quotations
$search = null;
$param_id = null;

if ($param_key === 'id') {                              // id searched
    $param_id = (int)$param_value;

    $quots = $opiquotations->quotations_by_ids([$param_id]);
    $header = null;
    $params['ids'] = (string)$param_id;
}
else if ($param_key === 'ids') {                        // ids searched
    // Analyze params
    $ids = preg_replace('/(^[^0-9]+|[^0-9]+$)/', '', $param_value);  // strip left and right
    $ids = mb_split('[^\-0-9]+', $ids);  // split on numbers or ranges (number-number)

    for ($i = 0; $i < count($ids); $i++) {
        $id = &$ids[$i];
        $id = preg_replace('/(^[^0-9]+|[^0-9]+$)/', '', $id);  // strip left and right

        if ($id === '') {
            $id = '0';
        }

        $a = mb_split('-', $id);  // split on numbers

        $id = (int)$a[0];

        $count_a = count($a);
        if ($count_a > 1) {  // it is a range
            for ($j = (int)$a[0] + 1; $j <= (int)$a[$count_a - 1]; $j++) {
                $ids[] = $j;
            }
        }
    }

    unset($a);
    unset($i);
    unset($j);
    unset($count_a);
    unset($id);

    $ids = array_unique($ids, SORT_NUMERIC);
    sort($ids);
    if ((count($ids) > 0) && ($ids[0] === 0)) {
        array_shift($ids);
    }

    // Recompose numbers and ranges to display
    $a = array();
    $first = null;
    if (count($ids) > 0) {
        $first = $ids[0];
        for ($i = 0; $i < count($ids) - 1; $i++) {
            if ($ids[$i + 1] !== $ids[$i] + 1) {
                $a[] = ($first === $ids[$i]
                ? (string)$first
                : "$first-$ids[$i]");
                $first = $ids[$i + 1];
            }
        }
        $a[] = ($first === $ids[$i]
        ? (string)$first
        : "$first-$ids[$i]");

        unset($first);
        unset($i);
    }

    // Query
    $nb_quotations = $opiquotations->quotations_by_ids_nb($ids);
    $quots = $opiquotations->quotations_by_ids($ids,
                                               $nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>Numéro(s)</strong> '.implode(', ', $a).'&thinsp;: <strong>'.$nb_quotations.'</strong> résultat'.s($nb_quotations);
    $param_value = implode(',', $a);

    unset($a);
}
else if ($param_key === 'subject') {                    // subject searched
    $nb_quotations = $opiquotations->quotations_by_subject_nb($param_value);
    $quots = $opiquotations->quotations_by_subject($param_value,
                                                   $nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>Sujet</strong> &quot;'.htmlspecialchars($param_value).'&quot;&thinsp;: <strong>'.$nb_quotations.'</strong> résultat'.s($nb_quotations);
    $search = $param_value;
}
else if ($param_key === 'text') {                       // text searched
    $nb_quotations = $opiquotations->quotations_by_text_nb($param_value);
    $quots = $opiquotations->quotations_by_text($param_value,
                                                $nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>Texte</strong> &quot;'.htmlspecialchars($param_value).'&quot;&thinsp;: <strong>'.$nb_quotations.'</strong> résultat'.s($nb_quotations);
    $search = $param_value;
}
else if ($param_key === 'author') {                     // author searched
    $nb_quotations = $opiquotations->quotations_by_author_nb($param_value);
    $quots = $opiquotations->quotations_by_author($param_value,
                                                  $nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>Auteur</strong> &quot;'.htmlspecialchars($param_value).'&quot;&thinsp;: <strong>'.$nb_quotations.'</strong> résultat'.s($nb_quotations);
    $search = $param_value;
}
else if ($param_key === 'work') {                       // work searched
    $nb_quotations = $opiquotations->quotations_by_work_nb($param_value);
    $quots = $opiquotations->quotations_by_work($param_value,
                                                $nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>&OElig;uvre</strong> &quot;'.htmlspecialchars($param_value).'&quot;&thinsp;: <strong>'.$nb_quotations.'</strong> résultat'.s($nb_quotations);
    $search = $param_value;
}
else if ($param_key === 'nation') {                     // nation searched
  $nb_quotations = $opiquotations->quotations_by_nation_nb($param_value);
    $quots = $opiquotations->quotations_by_nation($param_value,
                                                  $nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>Maxime</strong> &quot;'.htmlspecialchars($param_value).'&quot;&thinsp;: <strong>'.$nb_quotations.'</strong> résultat'.s($nb_quotations);
    $search = $param_value;
}
else if ($param_key === 'translation') {                // translation searched
    $nb_quotations = $opiquotations->quotations_by_translation_nb($param_value);
    $quots = $opiquotations->quotations_by_translation($param_value,
                                                       $nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>Traduction</strong> &quot;'.htmlspecialchars($param_value).'&quot;&thinsp;: <strong>'.$nb_quotations.'</strong> résultat'.s($nb_quotations);
    $search = $param_value;
}
else if (($param_key === 'selection')
         && !empty($SELECTION_LABELS[$param_value])) {  // selection specified
    $nb_quotations = $opiquotations->quotations_by_selection_label_nb($SELECTION_LABELS[$param_value]);
    $quots = $opiquotations->quotations_by_selection_label($SELECTION_LABELS[$param_value],
                                                           $nb_by_page, nb_to_offset($nb_quotations));

    $SELECTION_LINKS = array('Facebook' => ' postées sur la page Facebook <strong><a class="icon-Facebook monospace" href="https://www.facebook.com/1OPiCitationParJour">1OPiCitationParJour</a></strong>',
                             'Twitter' => ' postées sur le compte Twitter <strong><a class="icon-Twitter monospace" href="https://twitter.com/OPiCitationJour">@OPiCitationJour</a></strong>',
                             'Web' => ' ayant un lien vers le <strong><a class="icon-Web no-decoration">Web</a></strong>');

    $header = 'Les <strong>'.$nb_quotations.'</strong> citations'.$SELECTION_LINKS[$param_value];
}
else if ($param_key === 'all_marked') {                 // all quotations marked
    $nb_quotations = $opiquotations->quotations_all_marked_nb();
    $quots = $opiquotations->quotations_all_marked($nb_by_page, nb_to_offset($nb_quotations));
    $header = 'Mes <strong>'.$nb_quotations.'</strong> citations <strong class="mark">favorites</strong>';
}
else if ($param_key === 'all') {                        // all quotations
    $nb_quotations = $opiquotations->quotations_all_nb();
    $quots = $opiquotations->quotations_all($nb_by_page, nb_to_offset($nb_quotations));
    $header = '<strong>Toutes</strong> les '.$nb_quotations.' citations';
}
else {                                                  // $NB_FOR_RANDOM quotations at random
    $quots = $opiquotations->quotations_by_random($NB_FOR_RANDOM);
    $header = '<strong>'.count($quots).'</strong> citations choisies <strong>aléatoirement</strong>';

    #DEBUG
    assert('count($quots) === $NB_FOR_RANDOM');
    #DEBUG_END
}


// Init other informations
$only_one = (count($quots) === 1);

$keywords = ['citation', 'citations', 'maxime', 'maximes', 'proverbe', 'proverbes', 'littérature', 'français'];

if ($only_one) {
    $title = 'OPiCitations n°'.$quots[0]->id().' &mdash; Dictionnaire de citations';

    $desc = 'OPiCitations n°'.$quots[0]->id().' :
'.htmlspecialchars($quots[0]->to_text());


    // Prepare keywords from quotation
    $i = 0;
    foreach ([$quots[0]->subject(),
              ($quots[0]->is_maxim()
               ? $quots[0]->nation()
               : $quots[0]->author()), $quots[0]->work()] as $keyword) {
        if ($keyword !== null) {
            if ($i < 2) {  // if subject, nation/author then also add pieces
                $a = explode(' ', $keyword);
                if (count($a) > 1) {
                    $a[] = $keyword;
                }
            }
            else {
                $a = [$keyword];
            }

            foreach ($a as $keyword) {
                $keywords[] = htmlspecialchars($keyword);
            }

            unset($a);
        }

        ++$i;
    }

    unset($i);
    unset($keyword);
}
else {
    $title = 'OPiCitations &mdash; Dictionnaire de citations';

    $desc = 'Dictionnaire de citations.';

    if ($header !== null) {
        $desc .= '
'.strip_tags($header);
    }
}

$desc_open_graph = OPiQuotations\html_text_cut($desc, 300);
$desc = OPiQuotations\html_text_cut($desc, 160);


// Set URL
$url_site = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/';
$url = $url_site;

if ($only_one) {
    $url .= '?id='.$quots[0]->id();
}
else {
    if ($param_key !== null) {
        $url .= '?'.$param_key;
        if ($param_value !== null) {
            $url .= '='.rawurlencode($param_value);
        }
    }
}

?><!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Olivier Pirson">
    <meta name="description" content="<?php echo $desc; ?>">
    <meta name="keywords" content="<?php echo implode(',', $keywords); ?>">
    <meta name="keywords" lang="en" content="quotation,quotations,maxim,maxims,proverb,proverbs,literature,French">

    <title><?php echo $title; ?></title>

    <link rel="stylesheet" type="text/css" href="/OPiCitations/public/css/style.min.css">
    <style title="highlight">/* Stylesheet to activate/deactivate highlight with JavaScript. */</style>

    <script src="/OPiCitations/public/js/OPiQuotations.automatic-min.js" async="async"></script>

    <link rel="icon" type="image/x-icon" href="/OPiCitations/favicon.ico">

    <link rel="canonical" href="<?php echo $url; ?>">

    <meta property="og:image" content="<?php echo $url_site; ?>public/img/OPiQuotations<?php

$all_is_maxim = !empty($quots) || ($param_key === 'nation');

if ($all_is_maxim) {
    foreach ($quots as $quot) {
        if (!$quot->is_maxim()) {
            $all_is_maxim = false;

            break;
        }
    }
    if ($all_is_maxim) {
        echo '-maxim';
    }
}

unset($all_is_maxim);

?>-256x256-t.png">
    <meta property="og:description" content="<?php echo $desc_open_graph; ?>">
    <meta property="og:title" content="<?php echo $title; ?>">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo $url; ?>">

    <meta name="msapplication-TileColor" content="#fdfdd0">
    <meta name="msapplication-square150x150logo" content="/OPiCitations/public/img/OPiQuotations-64x64-t.png">
  </head>
  <body>
    <div id="top"></div>

    <nav id="lists">
<?php

foreach ($LABELS as $key=>$label) {
    if (isset($lists[$key])) {
        echo '<section id="list-', $key, '" class="list" aria-haspopup="true">
  <header>
    <div class="close">&times;</div>
    <h2><span id="list-', $key, '-nb"></span> ', mb_strtolower($label), 's</h2>
  </header>
  <div id="list-', $key, '-container">Chargement&hellip;</div>
</section>
';
    }
}

unset($key);
unset($label);

?>
    </nav>

    <nav id="control-panel">
      <div id="control-panel-right-border"><div><span>&rarr;</span></div><div><span>&rarr;</span></div></div>
      <div id="control-panel-close" class="close">&times;</div>
<!--[if lt IE 9]>
      <header id="please-don-t-use-IE">
        <p>Je vous en supplie,<br>utilisez un <i>vrai</i> navigateur&nbsp;!</p>
        <p>
          Je préconise
          <strong><a href="https://www.mozilla.org/fr/firefox/">Firefox</a></strong>.
        </p>
      </header>
<![endif]-->
      <form class="big-buttons" action="#">
        <ul>
          <li><button onclick="document.location.assign('./'); return false;"><?php echo $NB_FOR_RANDOM; ?> choisies <strong>aléatoirement</strong></button></li>
          <li><button name="all"><strong>Toutes</strong> les citations</button></li>
          <li>
            <div>
              <ul>
                <li><button name="selection" value="Web" title="Toutes celles ayant un lien vers le Web"><span></span></button></li>
                <li><button name="selection" value="Twitter" title="Toutes celles postées sur le compte Twitter"><span>&#10102;</span></button></li>
                <li><button name="selection" value="Facebook" title="Toutes celles postées sur la page Facebook"><span>&#10102;</span></button></li>
                <li><button name="all_marked" title="Mes favorites"><span></span></button></li>
              </ul>
            </div>
          </li>
        </ul>
      </form>
      Recherche par
      <ul id="menu">
<?php

// HTML form inputs
foreach ($LABELS as $key=>$label) {
    echo '        <li class="', $key, '">
          <div>
            '.($key === 'ids'
               ? ''
               : 'ou ').'<label for="', $key, '">', $label, '</label>&thinsp;:
';
    if (isset($lists[$key])) {
        echo '            <button class="list-open">liste '
            .(in_array($key, array('author', 'work'))
              ? 'd&rsquo;'
              : 'de ').$label.'s</button>
';
    }
    echo '          </div>
          <form action="#">
            <input id="', $key, '" name="', $key, '" type="search"', (isset($params[$key])
                                                                      ? ' value="'.htmlspecialchars($params[$key]).'"'
                                                                      : null), ($key === 'ids'
                                                                                ? ' placeholder="Ex.: 260 ou 259-261 ou 260,3241"'
                                                                                : ''), '>
          </form>
        </li>
';
}

unset($LABELS);
unset($key);
unset($label);

?>
        <li>
<?php

if ($search !== null && $search !== '') {
    echo '          <button id="highlight-on-off" title="(Néglige certaines occurences utilisant certains caractères non-alphanumériques.)">Surligner</button>';
}

?>
          <a class="go-bottom" href="#bottom"><span>&darr;</span></a>
          <a class="go-top" href="#top"><span>&uarr;</span></a>
        </li>
      </ul>

      <footer>
        <div id="about">
          <div>
            <h2 id="about-open">&Agrave; propos de</h2>
            <div aria-haspopup="true">
              <div id="about-close" class="close">&times;</div>
              <h3>
                <a class="no-decoration no-prefix" href="<?php echo $url_site; ?>">O<span class="surname">Pi</span>Citations</a>
              </h3>
              <p class="right">version 03.00.00 &mdash; 25 avril 2021</p>
              <div>
                <p>
                  Ensemble de citations pour l&rsquo;essentiel jadis récoltées par mes soins au hasard de mes lectures.
                </p>
                <p>
                  (Accumulées dans un antique
                  <a href="http://www.opimedia.be/DS/grenier/Amiga/online-Amiga/?run=Citations">programme</a>
                  écrit en AMOS BASIC sur Amiga, et déjà réutilisées par ce vieux
                  <a href="http://www.opimedia.be/DS/grenier/MS-DOS/online-DOS/?run=Enre">programme</a>
                  écrit en C sous M$-DOS.)
                </p>
                <p>
                  Vous pouvez me contacter par e-mail pour me signaler toute erreur constatée,
                  ou pour toute autre raison&thinsp;:
                  <a rel="nofollow" class="URL" href="mailto:olivier.pirson.opi@gmail.com?subject=OPiCitations">olivier.pirson.opi@gmail.com</a>
                </p>
                <div class="sep">
                  <strong>&#10102;</strong> citation choisie aléatoirement <strong>postée chaque matin</strong>
                  <ul>
                    <li>
                      sur le compte Twitter
                      <a class="icon-Twitter monospace" href="https://twitter.com/OPiCitationJour">@OPiCitationJour</a>
                    </li>
                    <li>
                      sur la page Facebook
                      <a class="icon-Facebook monospace" href="https://www.facebook.com/1OPiCitationParJour">1OPiCitationParJour</a>
                    </li>
                  </ul>
                </div>
                <p class="sep">
                  Une version réduite de cette application Web n&rsquo;affichant qu&rsquo;une seule citation
                  (pouvant être utilisée en tant qu&rsquo;<span class="monospace">&lt;iframe&gt;</span>
                  pour l&rsquo;inclure dans une autre page Web)&thinsp;:
                  <strong><a class="one-OPiQuotation" href="une-OPiCitation.php" target="_blank">&#10102;une O<span class="surname">Pi</span>Citation</a></strong>
                </p>
                <p class="sep">
                  Application Web écrite en <abbr>PHP</abbr>/MySQL/<abbr>HTML</abbr>5/Sass/JavaScript, sous licence libre <abbr lang="en" title="GNU General Public License">GPL</abbr>.
                </p>
                <ul>
                  <li>
                    Sources complètes (avec le fichier de citations) sur Bitbucket&thinsp;:<br>
                    <strong><a lang="en" class="URL" href="https://bitbucket.org/OPiMedia/opiquotations">https://bitbucket.org/OPiMedia/opiquotations</a></strong>
                  </li>
                  <li><strong><a lang="en" href="http://www.opimedia.be/DS/webdev/PHP/OPiQuotations/docs/">Online <abbr>HTML</abbr> documentation</a></strong></li>
                </ul>
                <div class="sep ds-jf-opi">
                  D&rsquo;autres citations sur mes sites&thinsp;:
                  <p>
                    <a href="http://www.opimedia.be/DS/citations/"><img src="/OPiCitations/public/img/DS-t.png" width="80" height="34" alt="[DS]"></a>
                    <a href="http://www.opimedia.be/JF/citations/"><img src="/OPiCitations/public/img/JF-t.png" width="41" height="34" alt="[JF]"></a>
                    <a href="http://www.opimedia.be/citations/"><img src="/OPiCitations/public/img/OPi-t.png" width="41" height="34" alt="[OPi]"></a>
                  </p>
                </div>
              </div>
              <p>
                <a class="donate" href="http://www.opimedia.be/donate/"><img src="/OPiCitations/public/img/Faire-un-don-124x26-t.png" width="124" height="26" alt="[Faire un don]"></a>
                <a class="opi" href="http://www.opimedia.be/">Olivier <span class="surname">Pirson</span> O<span class="surname">Pi</span></a>
              </p>
            </div>
          </div>
        </div>
      </footer>
    </nav>

    <main>
      <h1>
        <a class="no-decoration no-prefix" href="<?php echo $url_site; ?>"><span>O<span class="surname">Pi</span>Citations</span></a>
      </h1>

      <header>
<?php

if ($header !== null) {
    echo '          ', $header;
}
echo '
          <small>(sur <strong>'.$opiquotations->nb().'</strong> citations)</small>
          <div class="clear"></div>
';


function print_page_links() {
    global $nb_quotations;
    global $num_page;

    $nb_page = nb_to_nb_page($nb_quotations);
    if ($nb_page < 2) {
        return;
    }

    // Get current params
    $params = explode('&', $_SERVER['QUERY_STRING']);
    $count_params = count($params);
    for ($i = 0; $i < $count_params; ++$i) {
        if (preg_match('/^page=/', $params[$i])) {
            unset($params[$i]);
        }
    }

    $params = explode('&', implode('&', $params));
    $params[] = 'page=toutes';

    // Print links
    echo '<div class="page-links">
  Pages&thinsp;:
  <ul>
    <li><a href="?'.implode('&', $params).'">Toutes</a>&thinsp;;</li>
';

    unset($params[count($params) - 1]);
    $params = explode('&', implode('&', $params));

    for ($i = 1; $i <= $nb_page; ++$i) {
        if ($i >= 2) {
            if ($i === 2) {
                $params[] = 'page=2';
            }
            else {
              $params[count($params) - 1] = 'page='.$i;
            }
        }
        echo ($i === $num_page
              ? '<li class="selected"><a>'.$i.'</a></li>
'
              : '<li><a href="?'.implode('&', $params).'">'.$i.'</a></li>
');
    }

    echo '  </ul>
</div>
';
}

print_page_links();

?>
      </header>
<?php

// Display quotations
foreach ($quots as $quot) {
    echo $quot->to_html($search, true, null,
                        'h2', 'une-OPiCitation.php',
                        'fr');
}

unset($quot);

?>
    </main>

    <nav id="right-panel" aria-haspopup="true">
      <a class="go-top" href="#top"><span>&uarr;</span></a>
      <a class="go-bottom" href="#bottom"><span>&darr;</span></a>
    </nav>

    <div id="bottom"></div>
  </body>
</html>
