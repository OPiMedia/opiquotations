<?php /* -*- coding: utf-8 -*- */

/** \file OPiQuotations-sharing.php
 * (April 5, 2019)
 *
 * \brief
 * Return list of sharing buttons in a HTML format.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2016, 2017, 2018, 2019 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiCitations
 */

if (!isset($_GET['id'])) {
    return;
}


require_once 'OPiQuotations/log.inc';

#DEBUG
if (true) {
    // Development configuration
    ini_set('display_errors', 'stdout');
    ini_set('display_startup_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, true);
    assert_options(ASSERT_WARNING, true);
    assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
    // Production configuration
    ini_set('display_errors', 'stderr');
    ini_set('display_startup_errors', 0);
    ini_set('html_errors', 0);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, false);
    assert_options(ASSERT_WARNING, false);
    assert_options(ASSERT_BAIL, false);

    set_error_handler('\OPiQuotations\error_handler');
#DEBUG
}
#DEBUG_END

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');


require_once 'OPiQuotations/OPiQuotations.inc';

$opiquotations = new OPiQuotations\OPiQuotations();


$id = (int)$_GET['id'];
$quot = $opiquotations->quotation_by_id($id);
if ($quot === null) {
    return;
}
$url = 'http://www.opimedia.be/OPiCitations/?id='.$id;

$twitter_max_length = 279;
$max_length = $twitter_max_length - 23 - 1;  // 23 for length of short url
$quot_short = $quot->to_text_twitter(null, null, $max_length);
$quot_short_length = mb_strlen($quot_short) + 24;
$quot_short .= ' '.$url;

$encoded_quot_short = rawurlencode($quot_short);

$encoded_quot_all = rawurlencode($quot->to_text(true, 'fr', $url, true, array('Web')));
$encoded_title = rawurlencode('OPiCitation n°'.$id
                              .(!$quot->is_maxim() && $quot->author()
                                ? ' ('.$quot->author().')'
                                : ($quot->is_maxim() && $quot->nation()
                                   ? ' (nation '.$quot->nation().')'
                                   : '')));
$encoded_url = rawurlencode($url);

unset($id);
unset($quot);
unset($url);

// e-mail, diaspora*, Facebook, LinkedIn, Twitter
?>
<li>
  <a rel="nofollow" href="/OPiCitations/public/php/simple-diaspora-sharing-button/selectpod.php?title=<?php
echo $encoded_title;
?>&amp;url=<?php
echo $encoded_url;
?>&amp;notes=<?php
echo $encoded_quot_all.rawurlencode('
');
?>" target="_blank" title="Partager sur diaspora*"><img src="/OPiCitations/public/img/diaspora-t.png" width="32" height="32" alt="Partager sur diaspora*"></a>
</li>
<li>
  <a rel="nofollow" href="mailto:?subject=<?php
echo $encoded_title;
?>&amp;body=<?php
echo $encoded_quot_all.rawurlencode('
');
?>" title="Envoyer par e-mail"><img src="/OPiCitations/public/img/sharing/Email-t.png" width="32" height="32" alt="Envoyer par e-mail"></a>
</li>
<li>
  <a rel="nofollow" href="https://www.facebook.com/sharer/sharer.php?u=<?php
echo $encoded_url;
?>" target="_blank" title="Partager sur Facebook"><img src="/OPiCitations/public/img/sharing/Facebook-t.png" width="32" height="32" alt="Partager sur Facebook"></a>
</li>
<li>
  <a rel="nofollow" href="https://www.linkedin.com/shareArticle?mini=true&amp;title=<?php
echo $encoded_title;
?>&amp;url=<?php
echo $encoded_url;
?>&amp;source=<?php
echo $encoded_url;
?>&amp;summary=<?php
echo $encoded_quot_all;
?>" target="_blank" title="Partager sur LinkedIn"><img src="/OPiCitations/public/img/sharing/LinkedIn-t.png" width="32" height="32" alt="Partager sur LinkedIn"></a>
</li>
<li>
  <a rel="nofollow" href="https://twitter.com/intent/tweet?text=<?php
echo $encoded_quot_short;
if ($quot_short_length + 21 <= $twitter_max_length) {
    echo '&amp;via=OPiCitationJour';  # ' via @OPiCitationJour': length 21
}
?>" target="_blank" title="Tweeter"><img src="/OPiCitations/public/img/sharing/Twitter-t.png" width="32" height="32" alt="Tweeter"></a>
</li>
<?php

#DEBUG
if (true) {
    echo '<li style="background-color:#f0f0f0; border:1px solid black; left:-3em; position:absolute; top:-1em; z-index:10001" onclick="this.style.display=\'none\';"><pre style="margin:0">
';
    echo '<div style="border-bottom:1px solid black">title: ';
    var_dump($encoded_title);
    echo '</div><div style="border-bottom:1px solid black">URL: ';
    var_dump($encoded_url);
    echo '</div><div style="border-bottom:1px solid black">short:<br>';
    var_dump($encoded_quot_short);
    echo '</div><div style="border-bottom:1px solid black">text:<br>';
    var_dump($encoded_quot_all);
    echo '</div></pre></li>
';
    echo '<li style="background-color:white; border:1px solid black; left:-3em; position:absolute; top:-2em; z-index:10002" onclick="this.style.display=\'none\';"><pre style="margin:0">
';
    echo '<div style="border-bottom:1px solid black">title: ';
    var_dump(rawurldecode($encoded_title));
    echo '</div><div style="border-bottom:1px solid black">URL: ';
    var_dump(rawurldecode($encoded_url));
    echo '</div><div style="border-bottom:1px solid black">short:<br>';
    var_dump(rawurldecode($encoded_quot_short));
    echo '</div><div style="border-bottom:1px solid black">short normalized:<br>';
    $encoded_quot_short_normalized =  Normalizer::normalize($quot_short, Normalizer::FORM_C);
    var_dump($encoded_quot_short_normalized);
    echo '</div><div style="border-bottom:1px solid black">text:<br>';
    var_dump(rawurldecode($encoded_quot_all));
    echo '</div></pre></li>
';
}
#DEBUG_END

?>
<li class="close" onclick="sharingClose(this); return false;">&times;</li>
