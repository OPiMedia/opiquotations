<?php /* -*- coding: utf-8 -*- */

/** \file une-OPiCitation-pour-Facebook.php
 * (September 13, 2017)
 *
 * \brief
 * Little PHP application to send one quotation (from OPiCitations)
 * to Facebook account https://www.facebook.com/1OPiCitationParJour
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014, 2016, 2017 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiCitations
 */

set_include_path(get_include_path().PATH_SEPARATOR.dirname(realpath(__FILE__)));
require_once 'OPiQuotations/log.inc';

#DEBUG
if (true) {
    // Development configuration
    ini_set('display_errors', 'stdout');
    ini_set('display_startup_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, true);
    assert_options(ASSERT_WARNING, true);
    assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
    // Production configuration
    ini_set('display_errors', 'stderr');
    ini_set('display_startup_errors', 0);
    ini_set('html_errors', 0);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, false);
    assert_options(ASSERT_WARNING, false);
    assert_options(ASSERT_BAIL, false);

    set_error_handler('\OPiQuotations\error_handler');
#DEBUG
}
#DEBUG_END

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');

require_once 'OPiQuotations/OPiQuotations.inc';


$LABEL = 'Facebook - 1OPiCitationParJour';

// Choose a quotation at random
$opiquotations = new OPiQuotations\OPiQuotations();

$quot = $opiquotations->quotation_by_random($LABEL);
#DEBUG
#$quot = $opiquotations->quotation_by_id(9);
#$quot = $opiquotations->quotation_by_id(110);
#$quot = $opiquotations->quotation_by_id(459);
#$quot = $opiquotations->quotation_by_id(3062);
#DEBUG_END

if ($quot === null) {
    OPiQuotations\to_log('une-OPiCitation-pour-Facebook.php: no random quotation!');

    return false;
}

$text = $quot->to_text_facebook('http://www.opimedia.be/OPiCitations/?id='.$quot->id(),
                                'fr', array('Web'));

#DEBUG
if (true) {
    echo mb_strlen($text).'<pre>'.htmlspecialchars($text).'</pre>';

    return false;  // exit to *don't* post the message when do tests
}
#DEBUG_END


// Connect and send to Facebook
require_once('.private/Facebook_login.inc');

$ch = curl_init('https://graph.facebook.com/'.$page_id.'/feed');
if (!$ch) {
    OPiQuotations\to_log('une-OPiCitation-pour-Facebook.php curl_init error');

    return false;
}

$r = curl_setopt_array($ch,
                       array(CURLOPT_POST => 1,
                             CURLOPT_POSTFIELDS => array('access_token' => $access_token,
                                                         'message' => $text),
                             CURLOPT_RETURNTRANSFER => 1,
                             CURLOPT_SSL_VERIFYPEER => false));
if ($ch === false) {
    OPiQuotations\to_log('une-OPiCitation-pour-Facebook.php curl_setopt_array error '.curl_errno($ch).' : '.curl_error($ch));

    return false;
}

$result = curl_exec($ch);
if ($result === false) {
    OPiQuotations\to_log('une-OPiCitation-pour-Facebook.php curl_exec error '.curl_errno($ch).' : '.curl_error($ch));

    return false;
}

curl_close($ch);

$result = json_decode($result, true);

if (($result === null) || isset($result['error'])) {
    OPiQuotations\to_log('une-OPiCitation-pour-Facebook.php Facebook error '.print_r($result, true));

    return false;
}


if (empty($result['id'])) {
    OPiQuotations\to_log('une-OPiCitation-pour-Facebook.php: no add selection! '.$quot->id());
}
else {
    // Update DB with URL
    $msg_id = explode('_', $result['id']);
    $msg_id = array_pop($msg_id);

    $url = 'https://www.facebook.com/1OPiCitationParJour/posts/'.$msg_id;
    #DEBUG
    if (true) {
        echo '<pre>id:
';
        var_dump($result['id']);
        echo '</pre><a href="', $url, '">', $url, '</a>';
    }
    #DEBUG_END

    $selection = new OPiQuotations\Selection($LABEL, NULL, $url);

    $ok = $opiquotations->quotation_add_selection($quot, $selection);

    if (!$ok) {
        OPiQuotations\to_log('une-OPiCitation-pour-Facebook.php: add selection failed! '.$quot->id());
    }
}

?>