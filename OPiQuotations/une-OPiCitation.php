<?php /* -*- coding: utf-8 -*- */

/** \file une-OPiCitation.php
 * (November 23, 2018)
 *
 * \brief
 * Little Web application to display one quotation (from OPiCitations)
 * http://www.opimedia.be/OPiCitations/une-OPiCitation.php
 *
 * May be used in an iframe to include in another Web page.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2014, 2015, 2016, 2018 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiCitations
 */

require_once 'OPiQuotations/log.inc';

#DEBUG
if (true) {
    // Development configuration
    ini_set('display_errors', 'stdout');
    ini_set('display_startup_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, true);
    assert_options(ASSERT_WARNING, true);
    assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
    // Production configuration
    ini_set('display_errors', 'stderr');
    ini_set('display_startup_errors', 0);
    ini_set('html_errors', 0);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, false);
    assert_options(ASSERT_WARNING, false);
    assert_options(ASSERT_BAIL, false);

    set_error_handler('\OPiQuotations\error_handler');
#DEBUG
}
#DEBUG_END

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');


require_once 'OPiQuotations/OPiQuotations.inc';

$opiquotations = new OPiQuotations\OPiQuotations();


// Get quotation
if (isset($_GET['id'])) {  // specific quotation
    // GET correct id parameter
    $id = (int)$_GET['id'];

    $quot = ((string)$id === $_GET['id']
             ? $opiquotations->quotation_by_id($id)
             : null);

    unset($id);
}
else {                     // random quotation
    $quot = $opiquotations->quotation_by_random();
}

if ($quot === null) {  // incorrect argument or quotation not founded
    header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
    // Warning: possible infinite loop!

    exit;
}


// Init other informations
$title = 'une OPiCitations n°'.$quot->id().' &mdash; Dictionnaire de citations';

$desc = 'une OPiCitation n°'.$quot->id().' :
'.htmlspecialchars($quot->to_text());

$desc_open_graph = OPiQuotations\html_text_cut($desc, 300);
$desc = OPiQuotations\html_text_cut($desc, 160);


// Prepare keywords from quotation
$keywords = ['citation', 'citations', 'maxime', 'maximes', 'proverbe', 'proverbes', 'littérature', 'français'];

$i = 0;
foreach ([$quot->subject(), ($quot->is_maxim()
                             ? $quot->nation()
                             : $quot->author()), $quot->work()] as $keyword) {
    if ($keyword !== null) {
        if ($i < 2) {  // if subject, nation/author then also add pieces
            $a = explode(' ', $keyword);
            if (count($a) > 1) {
                $a[] = $keyword;
            }
        }
        else {
            $a = [$keyword];
        }

        foreach ($a as $keyword) {
            $keywords[] = htmlspecialchars($keyword);
        }

        unset($a);
    }

    ++$i;
}

unset($i);
unset($keyword);


// Set URL
$url_site = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/';
$url = $url_site.basename($_SERVER['PHP_SELF']).'?id='.$quot->id();
$url_opicitations = $url_site.'?id='.$quot->id();

?><!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Olivier Pirson">
    <meta name="description" content="<?php echo $desc; ?>">
    <meta name="keywords" content="<?php echo implode(',', $keywords); ?>">
    <meta name="keywords" lang="en" content="quotation,quotations,maxim,maxims,proverb,proverbs,literature,French">

    <title><?php echo $title; ?></title>

    <link rel="stylesheet" type="text/css" href="/OPiCitations/public/css/one-OPiQuotation.min.css">

    <script src="/OPiCitations/public/js/one-OPiQuotation.automatic-min.js" async="async"></script>

    <link rel="icon" type="image/x-icon" href="/OPiCitations/public/img/one-OPiQuotation-32x32.ico">

    <link rel="canonical" href="<?php echo $url; ?>">

    <meta property="og:image" content="<?php echo $url_site; ?>public/img/one-OPiQuotation<?php

if ($quot->is_maxim()) {
  echo '-maxim';
}

?>-256x256-t.png">
    <meta property="og:description" content="<?php echo $desc_open_graph; ?>">
    <meta property="og:title" content="<?php echo $title; ?>">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo $url; ?>">

    <meta name="msapplication-TileColor" content="#fdfdd0">
    <meta name="msapplication-square150x150logo" content="/OPiCitations/public/img/one-OPiQuotation-64x64-t.png">
  </head>
  <body>
    <main>
<?php

// Display quotation
echo $quot->to_html(null, true, '_blank',
                    'h1', 'une-OPiCitation.php',
                    'fr');

?>
    </main>
<?php if (!isset($_GET['no-link-OPiQuotations'])): ?>
    <footer>
      <a href="<?php echo $url_site; ?>" target="_blank">D&rsquo;autres citations sur <span>O<span class="surname">Pi</span>Citations</span></a>
    </footer>
  </body>
<?php endif; ?>
</html>
