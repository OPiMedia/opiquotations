<?php /* -*- coding: utf-8 -*- */

/** \file OPiQuotations-list.php
 * (September 3, 2016)
 *
 * \brief
 * Return list of authors, nations, subjects or works
 * with their number of occurrences,
 * in a JSON format.
 *
 * Piece of OPiQuotations.
 * https://bitbucket.org/OPiMedia/opiquotations
 *
 * GPLv3 --- Copyright (C) 2015, 2016 Olivier Pirson
 * http://www.opimedia.be/
 *
 * @package OPiCitations
 */

if (!isset($_GET['list'])) {
    return;
}


require_once 'OPiQuotations/log.inc';

#DEBUG
if (true) {
    // Development configuration
    ini_set('display_errors', 'stdout');
    ini_set('display_startup_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, true);
    assert_options(ASSERT_WARNING, true);
    assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
    // Production configuration
    ini_set('display_errors', 'stderr');
    ini_set('display_startup_errors', 0);
    ini_set('html_errors', 0);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, false);
    assert_options(ASSERT_WARNING, false);
    assert_options(ASSERT_BAIL, false);

    set_error_handler('\OPiQuotations\error_handler');
#DEBUG
}
#DEBUG_END

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');


require_once 'OPiQuotations/OPiQuotations.inc';

$opiquotations = new OPiQuotations\OPiQuotations();


if ($_GET['list'] === 'authors') {
    $list = $opiquotations->list_authors();
}
else if ($_GET['list'] === 'nations') {
    $list = $opiquotations->list_nations();
}
else if ($_GET['list'] === 'subjects') {
    $list = $opiquotations->list_subjects();
}
else if ($_GET['list'] === 'works') {
    $list = $opiquotations->list_works();
}
else {
    return;
}


$count_list = count($list);

#DEBUG
assert('$count_list > 0');
#DEBUG_END

if ($count_list > 0) {
    // Construct a PHP array in the correct order
    $array = [];
    foreach($list as $row) {
        $array[] = $row;
    }

    // Write JSON representation
    $json = json_encode($array);
    if ($json !== false) {
        echo $json;
    }
}

?>