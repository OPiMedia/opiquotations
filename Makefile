# Makefile of OPiQuotations --- 2018-11-23

.SUFFIXES:
.SUFFIXES: .css .sass

VERSION = 03_00_00

###########
# Options #
###########
CHECKTXT = checkTxt.pl

DOXYGEN = doxygen

PHPstripDEBUG = PHPstripDEBUG.py

SASS = sass --sourcemap=none -E utf-8

VNU      = vNu.sh  # java -jar vnu.jar  # https://bitbucket.org/OPiMedia/vnu-scripts and https://validator.github.io/validator/
VNUFLAGS =

YUICOMPRESSOR = java -jar ~/bin/yuicompressor-2.4.8.jar  # http://yui.github.io/yuicompressor/


CAT    = cat
CD     = cd
CHMOD  = chmod
CP     = cp
ECHO   = echo
GZIP   = gzip
MAKE   = make
MKDIR  = mkdir -p
MV     = mv
RM     = rm -f
RMDIR  = rmdir
SED    = sed -r
SHELL  = sh
TAR    = tar
TOUCH  = touch



########
# Main #
########
.PHONY:	all checkTxt css dists docs js stripped validate validateCss validateHtml

all:	css js

checkTxt:
	$(CHECKTXT) +lisible -put +questions -FOPiQuotations/favicon.ico -F_third/twitteroauth-0.6.4/LICENSE.md -E~$$ -E^_dist/ -E^_tmp/ -E^_img/ -E^MySQL/db_opiquotations_graph\. -E^stuffs/ .* * */.* */* */*/*
	$(CAT) OPiQuotations/logs/log.txt

css:	OPiQuotations/public/css/one-OPiQuotation.css OPiQuotations/public/css/style.css \
	OPiQuotations/public/css/one-OPiQuotation.min.css OPiQuotations/public/css/style.min.css

js:	OPiQuotations/public/js/OPiQuotations.automatic-min.js OPiQuotations/public/js/one-OPiQuotation.automatic-min.js

dists:	docs stripped
	$(MKDIR) _tmp/OPiQuotations/.private
	$(CP) -t _tmp/OPiQuotations/.private/ OPiQuotations/.private/Facebook_login.inc OPiQuotations/.private/Twitter_login.inc
	$(MKDIR) _tmp/OPiQuotations/OPiQuotations/.private
	$(CP) -t _tmp/OPiQuotations/OPiQuotations/.private/ OPiQuotations/OPiQuotations/.private/db_login.inc OPiQuotations/OPiQuotations/.private/log_email.inc

	$(MKDIR) _tmp/stripped/OPiQuotations/.private
	$(CP) -t _tmp/stripped/OPiQuotations/.private/ OPiQuotations/.private/Facebook_login.inc OPiQuotations/.private/Twitter_login.inc
	$(MKDIR) _tmp/stripped/OPiQuotations/OPiQuotations/.private
	$(CP) -t _tmp/stripped/OPiQuotations/OPiQuotations/.private/ OPiQuotations/OPiQuotations/.private/db_login.inc OPiQuotations/OPiQuotations/.private/log_email.inc

	$(CP) -t _tmp/OPiQuotations README.rst
	$(MKDIR) _tmp/OPiQuotations/libs
	$(CP) -R OPiQuotations/libs/* _tmp/OPiQuotations/libs
	$(MKDIR) _tmp/OPiQuotations/COPYING
	$(CP) -t _tmp/OPiQuotations/COPYING OPiQuotations/COPYING/*

	$(CP) -t _tmp/stripped/OPiQuotations README.rst
	$(MKDIR) _tmp/OPiQuotations/libs
	$(CP) -R OPiQuotations/libs/* _tmp/OPiQuotations/libs
	$(MKDIR) _tmp/stripped/OPiQuotations/COPYING
	$(CP) -t _tmp/stripped/OPiQuotations/COPYING OPiQuotations/COPYING/*

	$(MKDIR) _tmp/OPiQuotations/logs
	$(TOUCH) _tmp/OPiQuotations/logs/log.txt

	$(MKDIR) _tmp/stripped/OPiQuotations/logs
	$(TOUCH) _tmp/stripped/OPiQuotations/logs/log.txt

	$(MKDIR) _tmp/OPiQuotations/public
	$(CP) -R OPiQuotations/public/* _tmp/OPiQuotations/public

	$(MKDIR) _tmp/stripped/OPiQuotations/public
	$(CP) -R OPiQuotations/public/* _tmp/stripped/OPiQuotations/public

	$(MV) _tmp/stripped/OPiQuotations _tmp/stripped/OPiQuotations_stripped
	$(MV) _tmp/docs _tmp/OPiQuotations_html

	$(MKDIR) _tmp/OPiQuotations_mysql/COPYING
	$(CP) -t _tmp/OPiQuotations_mysql README.rst
	$(CP) -t _tmp/OPiQuotations_mysql/COPYING OPiQuotations/COPYING/*
	-$(CP) MySQL/* _tmp/OPiQuotations_mysql/
	$(RM) _tmp/OPiQuotations_mysql/db_opiquotations_graph.mwb

	$(CD) _tmp/stripped; $(TAR) -cvf ../OPiQuotations_stripped.tar OPiQuotations_stripped
	$(MV) _tmp/stripped/OPiQuotations_stripped _tmp/stripped/OPiQuotations

	$(CD) _tmp; $(TAR) -cvf OPiQuotations.tar OPiQuotations

	$(CD) _tmp; $(TAR) -cvf OPiQuotations_html.tar OPiQuotations_html
	$(MV) _tmp/OPiQuotations_html _tmp/docs

	$(CD) _tmp; $(TAR) -cvf OPiQuotations_mysql.tar OPiQuotations_mysql

	$(CD) _tmp; $(GZIP) -9 --no-name OPiQuotations.tar
	$(MV) _tmp/OPiQuotations.tar.gz _tmp/OPiQuotations_$(VERSION).tar.gz

	$(CD) _tmp; $(GZIP) -9 --no-name OPiQuotations_stripped.tar
	$(MV) _tmp/OPiQuotations_stripped.tar.gz _tmp/OPiQuotations_$(VERSION)_stripped.tar.gz

	$(CD) _tmp; $(GZIP) -9 --no-name OPiQuotations_html.tar
	$(MV) _tmp/OPiQuotations_html.tar.gz _tmp/OPiQuotations_$(VERSION)_html.tar.gz

	$(CD) _tmp; $(GZIP) -9 --no-name OPiQuotations_mysql.tar
	$(MV) _tmp/OPiQuotations_mysql.tar.gz _tmp/OPiQuotations_$(VERSION)_mysql.tar.gz

	$(GZIP) -t _tmp/OPiQuotations_$(VERSION).tar.gz
	$(GZIP) -t _tmp/OPiQuotations_$(VERSION)_stripped.tar.gz
	$(GZIP) -t _tmp/OPiQuotations_$(VERSION)_html.tar.gz
	$(GZIP) -t _tmp/OPiQuotations_$(VERSION)_mysql.tar.gz

docs:
	$(MKDIR) _tmp/docs
	$(CP) -t _tmp/docs/ MySQL/db_opiquotations_graph.png _img/Donate-92x26-t.png _img/OPiCitation-banner--577x100.jpg _img/one-OPiQuotation-16x16-t.png _img/OPiQuotations-16x16-t.png
	$(CD) OPiQuotations; $(DOXYGEN)
	$(CD) _tmp/docs/; $(CHMOD) uog+r *

stripped:
	$(MKDIR) _tmp/OPiQuotations/OPiQuotations/.private
	$(CP) -t _tmp/OPiQuotations/ OPiQuotations/*.php
	$(CP) -t _tmp/OPiQuotations/OPiQuotations/ OPiQuotations/OPiQuotations/*.inc
	$(CD) _tmp; $(PHPstripDEBUG) OPiQuotations -r -E utf_8 -d stripped -h html -q

validate:	validateCss validateHtml

validateCss:
	-@$(VNU) $(VNUFLAGS) --skip-non-css OPiQuotations/public/css/style.css OPiQuotations/public/css/one-OPiQuotation.css

validateHtml:
	-@$(VNU) $(VNUFLAGS) http://www.opimedia.be/OPiCitations/ http://www.opimedia.be/OPiCitations/une-OPiCitation.php



#########
# Rules #
#########
.PRECIOUS:	%.css

%.automatic-min.js:	%.js
	$(SED) 's/console\.assert(.+?);//' $< \
	| $(YUICOMPRESSOR) $(VERBOSE) --type js --charset utf-8 --nomunge -o $@

OPiQuotations/public/css/%.css:	sass/%.sass
	$(SASS) $< $@

OPiQuotations/public/css/%.min.css:	sass/%.sass
	$(SASS) --style compressed $< $@



#########
# Clean #
#########
.PHONY:	clean distclean

clean:
	$(RM) -r .sass-cache
	$(RM) -r _tmp
	$(RM) -r checkTxt.log

distclean:	clean
