-- -*- coding: utf-8 -*-

-- Create procedures in database opiquotations.
-- (January 7, 2014)
--
-- (Not used.)
--
-- Piece of OPiQuotations.
-- https://bitbucket.org/OPiMedia/opiquotations
--
-- GPLv3 --- Copyright (C) 2014 Olivier Pirson
-- http://www.opimedia.be/
-- ---------------------------------------------

SELECT 'DATABASE opiquotations';

USE `opiquotations`;



DROP PROCEDURE IF EXISTS `proc_ltrim_not_alphanum`;

-- Returns text with leading non-alphabetic characters removed.
--
-- Return: SELECT TEXT
-- (Not used.)
-- --------------------
DELIMITER //
CREATE PROCEDURE `proc_ltrim_not_alphanum`
       (text TEXT)
BEGIN
      DECLARE i INT UNSIGNED;
      DECLARE len INT UNSIGNED;

      SET i = 1;
      SET len = LENGTH(text);

      loop_i: WHILE i <= len DO
              IF SUBSTRING(text, i, 1) COLLATE utf8_unicode_ci REGEXP '[A-Z0-9Ç]' THEN
                  SET text = SUBSTRING(text, i);

                  LEAVE loop_i;
                  IF SUBSTRING(text, i, 1) = '<' THEN  -- skip HTML tag
                      WHILE (i < len) AND (SUBSTRING(text, i, 1) != '>') DO
                          SET i = i + 1;
                      END WHILE;
                  END IF;
              END IF;

              SET i = i + 1;
      END WHILE;

      SELECT text;
END;
//
DELIMITER ;
