-- -*- coding: utf-8 -*-

-- Create views in database opiquotations.
-- (March 14, 2018)
--
-- Piece of OPiQuotations.
-- https://bitbucket.org/OPiMedia/opiquotations
--
-- GPLv3 --- Copyright (C) 2014, 2016, 2018 Olivier Pirson
-- http://www.opimedia.be/
-- ---------------------------------------------

SELECT 'DATABASE opiquotations';

USE `opiquotations`;



-- View vw_quotation
-- -----------------
CREATE OR REPLACE VIEW `vw_quotation`
AS
SELECT `q`.`id`, `q`.`text`, `q`.`text_stripped`, `q`.`text_lang`, `q`.`translation`, `q`.`translation_stripped`, `q`.`is_maxim`, `q`.`is_marked`, `q`.`is_misattributed`,
       `s`.`name` AS `subject`,
       `n`.`name` AS `nation`,
       `a`.`name` AS `author`,
       `w`.`name` AS `work`
FROM `quotation` AS `q`
LEFT OUTER JOIN `subject` AS `s` ON `q`.`subject_id`=`s`.`id`
LEFT OUTER JOIN `nation` AS `n` ON `q`.`is_maxim` AND `q`.`nation_author_id`=`n`.`id`
LEFT OUTER JOIN `author` AS `a` ON NOT `q`.`is_maxim` AND `q`.`nation_author_id`=`a`.`id`
LEFT OUTER JOIN `work` AS `w` ON `q`.`work_id`=`w`.`id`;


-- View vw_quotation_selection
-- ---------------------------
CREATE OR REPLACE VIEW `vw_quotation_selection`
AS
SELECT `q`.*,
       `s`.`label` AS `selection_label`, `datetime_utc` AS `selection_datetime_utc`, `s`.`url` AS `selection_url`
FROM `vw_quotation` AS `q`
LEFT JOIN `quotation_selection` AS `s` ON `q`.`id`=`s`.`quotation_id`
ORDER BY `q`.`id`, `selection_datetime_utc`;
