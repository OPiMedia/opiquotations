-- -*- coding: utf-8 -*-

-- Some SQL commands useful to this project.
-- (February 6, 2014)
--
-- (Not used.)
--
-- Piece of OPiQuotations.
-- https://bitbucket.org/OPiMedia/opiquotations
--
-- GPLv3 --- Copyright (C) 2014 Olivier Pirson
-- http://www.opimedia.be/
-- ---------------------------------------------

SELECT 'DATABASE opiquotations';

USE `opiquotations`;



-- Copy stripped `text` to `text_stripped`
-- ---------------------------------------
UPDATE `quotation` AS `q`
SET `q`.`text_stripped`=upperstrip_not_alphanum(`q`.`text`);



-- Copy stripped `translation` to `translation_stripped`
-- -----------------------------------------------------
UPDATE `quotation` AS `q`
SET `q`.`translation_stripped`=upperstrip_not_alphanum(`q`.`translation`)
WHERE `q`.`translation` IS NOT NULL;
