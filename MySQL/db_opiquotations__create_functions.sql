-- -*- coding: utf-8 -*-

-- Create function in database opiquotations.
-- (January 7, 2014)
--
-- (Required SUPER USER privilege to define.)
-- (Not used.)
--
-- Piece of OPiQuotations.
-- https://bitbucket.org/OPiMedia/opiquotations
--
-- GPLv3 --- Copyright (C) 2014 Olivier Pirson
-- http://www.opimedia.be/
-- ---------------------------------------------

SELECT 'DATABASE opiquotations';

USE `opiquotations`;



DROP FUNCTION IF EXISTS `ltrim_not_alphanum`;

-- Returns text with leading non-alphanumeric characters removed.
--
-- (Required SUPER USER privilege to define.)
-- (Not used.)
-- ------------
DELIMITER //
CREATE FUNCTION `ltrim_not_alphanum`
       (text TEXT)
       RETURNS TEXT
NO SQL
BEGIN
      DECLARE i INT UNSIGNED;
      DECLARE len INT UNSIGNED;

      SET i = 1;
      SET len = LENGTH(text);

      WHILE i <= len DO
            IF SUBSTRING(text, i, 1) COLLATE utf8_unicode_ci REGEXP '[A-Z0-9Ç]' THEN
                RETURN SUBSTRING(text, i);
            ELSE
                IF SUBSTRING(text, i, 1) = '<' THEN  -- skip HTML tag
                    WHILE (i < len) AND (SUBSTRING(text, i, 1) != '>') DO
                        SET i = i + 1;
                    END WHILE;
                END IF;
            END IF;

            SET i = i + 1;
      END WHILE;

      RETURN text;
END;
//
DELIMITER ;



DROP FUNCTION IF EXISTS `upperstrip_not_alphanum`;

-- Returns text with all non-alphanumeric characters removed.
--
-- (Required SUPER USER privilege to define.)
-- (Not used.)
-- ------------
DELIMITER //
CREATE FUNCTION `upperstrip_not_alphanum`
       (text TEXT)
       RETURNS TEXT
NO SQL
BEGIN
      DECLARE i INT UNSIGNED;
      DECLARE len INT UNSIGNED;
      DECLARE s TEXT;

      SET i = 1;
      SET len = LENGTH(text);
      SET s = '';

      WHILE i <= len DO
            IF SUBSTRING(text, i, 1) COLLATE utf8_unicode_ci REGEXP '[A-Z0-9]' THEN
               SET s = CONCAT(s, UPPER(SUBSTRING(text, i, 1)));
            ELSE
               SET s = CONCAT(s, (SELECT CASE SUBSTRING(text, i, 1) COLLATE utf8_unicode_ci
                                         WHEN 'à' THEN 'A'
                                         WHEN 'ç' THEN 'C'
                                         WHEN 'é' THEN 'E'
                                         WHEN 'ï' THEN 'I'
                                         WHEN 'ñ' THEN 'N'
                                         WHEN 'ô' THEN 'O'
                                         WHEN 'ù' THEN 'U'
                                         ELSE ''
                                  END));
                IF SUBSTRING(text, i, 1) = '<' THEN  -- skip HTML tag
                    WHILE (i < len) AND (SUBSTRING(text, i, 1) != '>') DO
                        SET i = i + 1;
                    END WHILE;
                END IF;
            END IF;

            SET i = i + 1;
      END WHILE;

      RETURN s;
END;
//
DELIMITER ;

-- To test
-- SELECT CONCAT_WS('\n', upperstrip_not_alphanum('  AÁÀÂÄaáàâä|Çc|EÉÈÊËeéèêë|IÍÌÎÏiíìîï|Ññ|OÓÒÔöoóòôö|UÚÙÛÜuúùûü  '),
--                        upperstrip_not_alphanum('  AAAAAAAAAA|CC|EEEEEEEEEE|IIIIIIIIII|NN|OOOOOOOOOO|UUUUUUUUUU  '));
