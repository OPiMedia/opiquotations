-- Create database opiquotations.
-- (January 7, 2014)
--
-- Piece of OPiQuotations.
-- https://bitbucket.org/OPiMedia/opiquotations
--
-- GPLv3 --- Copyright (C) 2014 Olivier Pirson
-- http://www.opimedia.be/
-- ---------------------------------------------
SET sql_mode = 'STRICT_ALL_TABLES';

SELECT 'DATABASE opiquotations';

DROP DATABASE IF EXISTS `opiquotations`;

CREATE DATABASE `opiquotations`
       CHARACTER SET utf8
       COLLATE utf8_bin;
