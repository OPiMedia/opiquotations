-- Create tables in database opiquotations.
-- (March 14, 2018)
--
-- Piece of OPiQuotations.
-- https://bitbucket.org/OPiMedia/opiquotations
--
-- GPLv3 --- Copyright (C) 2014, 2016, 2018 Olivier Pirson
-- http://www.opimedia.be/
-- ---------------------------------------------
SET sql_mode = 'STRICT_ALL_TABLES';

SELECT 'DATABASE opiquotations';

USE `opiquotations`;



-- Author
-- ------
SELECT 'author';

CREATE TABLE `author` (
       `id`           INT UNSIGNED      AUTO_INCREMENT PRIMARY KEY,

       `name`         VARCHAR(100)      NOT NULL UNIQUE KEY
) ENGINE InnoDB
  CHARACTER SET utf8
  COLLATE utf8_bin;


-- Nation
-- ------
SELECT 'nation';

CREATE TABLE `nation` (
       `id`           INT UNSIGNED      AUTO_INCREMENT PRIMARY KEY,

       `name`         VARCHAR(100)      NOT NULL UNIQUE KEY
) ENGINE InnoDB
  CHARACTER SET utf8
  COLLATE utf8_bin;


-- Subject
-- -------
SELECT 'subject';

CREATE TABLE `subject` (
       `id`            INT UNSIGNED     AUTO_INCREMENT PRIMARY KEY,

       `name`          VARCHAR(100)     NOT NULL UNIQUE KEY
) ENGINE InnoDB
  CHARACTER SET utf8
  COLLATE utf8_bin;


-- Work
-- ----
SELECT 'work';

CREATE TABLE `work` (
       `id`         INT UNSIGNED        AUTO_INCREMENT PRIMARY KEY,

       `name`       VARCHAR(100)        NOT NULL UNIQUE KEY
) ENGINE InnoDB
  CHARACTER SET utf8
  COLLATE utf8_bin;



-- Quotation
-- ---------
SELECT 'quotation';

CREATE TABLE `quotation` (
       `id`                     INT UNSIGNED    AUTO_INCREMENT PRIMARY KEY,

       `text`                   TEXT            NOT NULL,
       `text_stripped`          CHAR(100)       NOT NULL UNIQUE KEY,
       `text_lang`              CHAR(5)         DEFAULT NULL,
       `translation`            TEXT,
       `translation_stripped`   CHAR(100)       UNIQUE KEY,
       `is_maxim`               BOOL            NOT NULL,
       `is_marked`              BOOL            NOT NULL,
       `subject_id`             INT UNSIGNED,
       `nation_author_id`       INT UNSIGNED,
       `work_id`                INT UNSIGNED,
       `is_misattributed`       BOOL            NOT NULL,
       `added_timestamp`        DATETIME        DEFAULT CURRENT_TIMESTAMP,

       CONSTRAINT `fk_quotation_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
       CONSTRAINT `fk_quotation_work_id` FOREIGN KEY (`work_id`) REFERENCES `work` (`id`)
       -- no foreign key on author and nation, because dealed with only one field
) ENGINE InnoDB
  CHARACTER SET utf8
  COLLATE utf8_bin;



-- List of quotations already selection (and associated with a label)
-- ------------------------------------------------------------------
SELECT 'quotation_selection';

CREATE TABLE IF NOT EXISTS `quotation_selection` (
       `quotation_id`      INT UNSIGNED      NOT NULL,

       `label`             CHAR(31)          NOT NULL,
       `datetime_utc`      DATETIME          NOT NULL,
       `url`               VARCHAR(255)      DEFAULT NULL,

       CONSTRAINT `fk_quotation_selection_quotation_id` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`),
       CONSTRAINT `pk_quotation_selection_quotation_id_label` PRIMARY KEY (`quotation_id`, `label`)
) ENGINE InnoDB
  CHARACTER SET utf8
  COLLATE utf8_bin;
