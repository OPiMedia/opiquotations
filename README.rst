.. -*- restructuredtext -*-

=============
OPiQuotations
=============

PHP package to deal quotations (text, author...) and maxims (text, nation...)
from a MySQL database.

* Complete sources (and MySQL quotations file) on Bitbucket: https://bitbucket.org/OPiMedia/opiquotations
* `Online HTML documentation`_

|OPiQuotations|

My personal use of this package:

|OPiCitations|

* `Web application containing a lot of French quotes`_ |OPiQuotations-16|
* `une OPiCitation`_ |one-OPiQuotation-16|: Little Web application to display one OPiCitation (may be used in an iframe to include in another Web page)
* `@OPiCitationJour`_: Twitter account with an OPiCitation by day
* `1OPiCitationParJour`_: Page Facebook with another OPiCitation by day

.. _`Online HTML documentation`: http://www.opimedia.be/DS/webdev/PHP/OPiQuotations/docs/
.. _`Web application containing a lot of French quotes`: http://www.opimedia.be/OPiCitations/
.. _`une OPiCitation`: http://www.opimedia.be/OPiCitations/une-OPiCitation.php
.. _`@OPiCitationJour`: https://twitter.com/OPiCitationJour
.. _`1OPiCitationParJour`: https://www.facebook.com/1OPiCitationParJour

.. |OPiCitations| image:: https://bitbucket.org/OPiMedia/opiquotations/raw/master/_img/OPiCitation-banner--577x100.jpg
.. |OPiQuotations| image:: https://bitbucket.org/OPiMedia/opiquotations/raw/master/_img/OPiQuotations-64x64-t.png
.. |OPiQuotations-16| image:: https://bitbucket.org/OPiMedia/opiquotations/raw/master/_img/OPiQuotations-16x16-t.png
.. |one-OPiQuotation-16| image:: https://bitbucket.org/OPiMedia/opiquotations/raw/master/_img/one-OPiQuotation-16x16-t.png

|



Message to developers
=====================
This is a **free software**, so you can download it, **modify it** and **submit your modifications**.
You can also **redistribute** your own version (keeping the GPL license).

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



Support me
==========
This package is a **free software** (GPL license).
It is **completely free** (like "free speech" *and* like "free beer").
However you can **support me** financially by donating.

Click to this link |Donate|
**Thank you!**

.. |Donate| image:: http://www.opimedia.be/donate/_png/Paypal_Donate_92x26_t.png
   :target: http://www.opimedia.be/donate/

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2014 – 2021 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png


Other parts
-----------
* `Comme Light`_ by `Vernon Adams`_
  (`SIL Open Font License`_).
  See also `Bug in dash character`.

.. _`Comme Light`: https://www.fontsquirrel.com/fonts/comme
.. _`SIL Open Font License`: https://www.fontsquirrel.com/license/comme
.. _`Vernon Adams`: https://plus.google.com/+vernonadams
.. _`Bug in dash character`: http://www.opimedia.be/DS/webdev/bugs-list/Bug-in-dash-character-of-Comme-Light-ttf-Vernon-Adams-font-with-recent-browsers/

* `Simple diaspora* sharing button`_
  (https://github.com/sebastienadam/simple_diaspora_sharing_button/blob/master/LICENSE.txt)

.. _`Simple diaspora* sharing button`: https://github.com/sebastienadam/simple_diaspora_sharing_button/

* `Social Flat Rounded Rects`_ icons by `Aha-Soft`_
  (`Creative Commons (Attribution 3.0 Unported)`_).

.. _`Aha-Soft`: http://www.aha-soft.com/
.. _`Creative Commons (Attribution 3.0 Unported)`: https://creativecommons.org/licenses/by/3.0/
.. _`Social Flat Rounded Rects`: https://www.iconfinder.com/iconsets/social-flat-rounded-rects

* TwitterOAuth_
  (https://github.com/abraham/twitteroauth/blob/master/LICENSE.md)

.. _TwitterOAuth: https://twitteroauth.com/



Known bug
=========
* iPod Safari: left panel doesn't appear when click it.



Changes
=======
* **03.00.00** (working version) — April, 2021

  - Added assertions in development JavaScript code.
  - Added `Db::to_string()` and `Db::to_string_or_NULL()` functions.
  - Added `is_misattributed` and `added_timestamp` fields in `quotation` table.
  - Added `OPiQuotations::quotation_add_selection()` and modified `OPiQuotations::quotations_by_random()`.
  - Added `OPiQuotations::quotation_by_random()` function.
  - Added language field to specify HTML lang="" when there is a translation.
  - Added **lists** of quotations **posted on Twitter and Facebook** and quotations with Web links.
    and link to the message on each of these quotations.
  - Added pagination mechanism.
  - Added `rel="nofollow"` attribute to each URL selection.
  - Added translation and some options in to_text*() methods.
  - Reindented code.
  - Renamed `_selected_label` table to `quotation_selection`, added `url` field and added `vw_quotation_selection` view.
  - Replaced "" quotes by “ ”.
  - Replaced float by flex boxes for two columns text|translation.
  - Replaced Yahoo email by GMail.
  - Removed Google+ sharing button.
  - Updated log.
  - Updated Twitter length to 280 characters.
  - Updated TwitterOAuth_ and adapted `une-OPiCitation-pour-Twitter.php`.

  - In Web application `index.php`:

    - Added **diaspora*, LinkedIn and email sharing buttons**
      and replaced intrusive sharing buttons by an AJAX panel with simple links.
    - Added mention of misattributed quotation.
    - Added pagination.
    - Customized responsive mode and general interface.
    - Removed JavaScript to old Internet Explorer.
    - Replaced "nation" by "maxime" in HTML pages and adapted CSS.
    - Replaced ' quotes by " in JavaScript.
    - Replaced TTF font by WOFF2 or WOFF.
    - Reseted button CSS for Firefox.

  - In Web application `une-OPiCitation.php`:

    - Added print style.
    - Added mention of misattributed quotation.
    - Corrected external style link.

  - In Web application `une-OPiCitation-pour-Twitter.php`:

    - Added 5 tries with decreasing length.

* 02.01.00 — August 29, 2016

  - Added **hashtag** to Twitter and Facebook posts.
  - Corrected `Comme-Light.ttf font bug`_ with a workaround.
  - Removed not necessary target="_blank".
  - Renamed oneOPiQuotation and uneOPiCitation files to one-OPiQuotation and une-OPiCitation.
  - Renamed `OPiQuotations_list.php` to `OPiQuotations-list.php`.
  - Renamed `uneOPiCitation_pour_Facebook.php` to `une-OPiCitation-pour-Facebook.php`.
  - Renamed `uneOPiCitation_pour_Twitter.php` to `une-OPiCitation-pour-Twitter.php`.
  - In Web application `index.php`:

    - Added style to links.
    - Added some background images and more visible border on reduced left panel.
    - Corrected JavaScript bug with Google+ sharing button.
    - Removed message and JavaScript to very old Internet Explorer.
    - Replaced `hover` by `onclick` on about panel.
    - Replaced `<span class="highlight">` by `<mark>`.

.. _`Comme-Light.ttf font bug`: http://www.opimedia.be/DS/webdev/bugs-list/Bug-in-dash-character-of-Comme-Light-ttf-Vernon-Adams-font-with-recent-browsers/

* 02.00.01 — August 1st, 2016

  - Corrected bug in `index.php`: random button run only once.
  - Corrected print style.

  - Replaced links used to sharing in une-OPiCitation.
  - Simplified print style.
  - Updated email link in `index.php`.

* **02.00.00** — October 26, 2015

  - Added cutting functions.
  - Added external id links to une-OPiCitation.
  - Added Open Graph attributes.
  - Added Twitter, Facebook and Google+ **share buttons**.
  - Corrected some minor bugs.
  - Customized interface.
  - Minified CSS and JavaScript.
  - Renamed some images.
  - Replaced hard lists of authors, nations, subjects and works by **AJAX lists**.
  - Replaced post form by get form.
  - Some cosmetic changes.

* 01.04.00 — October 8, 2015

  - Added **responsive mode**.
  - Added links to subject, nation, author and work items.
  - Improved HTML 5 and CSS.

* 01.03.01 — November 15, 2014

  - Added donate button on Web application and documentation.

* 01.03.00 — July 3, 2014

  - Added Twitter and Facebook links.
  - Added nofollow attribute to the id and list links.
  - Corrected CSS order.
  - Corrected some typos in DB.
  - Corrected wrong parameters in to_log() calls.
  - Hide password in log messages.
  - Improved JavaScript loading.
  - Removed Dina font.
  - Set button padding for some browsers.

* 01.02.00 — February 7, 2014

  - Added handler error function and possibility to send error log by mail.
  - Added lists in `index.php` and API.
  - Added OPiQuotations->quotations_by_random() function.
  - Added _selected_label table in DB.
  - Corrected / bug in highlight.
  - Corrected bug in Db->nb().
  - Extended OPiQuotations->query_quotations() function.
  - More robust OPiQuotations->quotation_by_id() function.
  - Renamed highlight.js to OPiQuotations.js.

* 01.01.00 — February 5, 2014

  - Added link in display of id quotation.
  - Added dealing of GET params in `index.php`.
  - Added OPiQuotation->to_text_twitter() method.
  - Added `une-OPiCitation-pour-Facebook.php` application.
  - Added `une-OPiCitation-pour-Twitter.php` application.

  - Generalized access to log file.

  - Corrected Bitbucket link problem with Chrome.
  - Corrected bug if empty ids.
  - Corrected some typos in DB.

* 01.00.01 — January 7, 2014

  - Added `text_stripped` and `translation_stripped` fields in DB for better sorting.
  - Added `vw_quotation` in DB.

* **01.00.00** — January 4, 2014

  - First public version.

* November 25, 2013: started.
